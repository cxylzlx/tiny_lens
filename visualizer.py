from RayTracing import Tracer
from matplotlib import pyplot as plt
import numpy as np 
import util
from analysis_tool import get_critical_line_and_caustic

def visualizer(fitter,params,positions=None):
    """
    fitter: fitter object
    params: fitting params results
    """
    thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(params)
    tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
    if fitter.sub_grid is None:
        best_image = tracer.generate_model_image(fitter.x,fitter.y)
    else:
        best_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
    chi_square_map = (fitter.ccd_image - best_image)**2 * fitter.weight

    xsrc,ysrc = tracer.ray_shoot(fitter.x,fitter.y)
    sgridx, sgridy, _, _ = util.generate_grid(500,0.01)
    I_source = np.zeros(sgridx.shape)
    for item in thissource:
        I_source += item.get_instensity(sgridx,sgridy)

    #alphax,alphay = tracer.total_deflection(fitter.x,fitter.y)
    if positions is not None:
        for pos_pair in positions:
            posx = np.array([item[0] for item in pos_pair])
            posy = np.array([item[1] for item in pos_pair])
            #print('posx',posx,'---','posy',posy)
            src_pair = [tracer.ray_shoot(item[0],item[1]) for item in pos_pair]
            #print('source pair',src_pair)
            #print('lens pair',pos_pair)
            srcx = np.array([item[0] for item in src_pair])
            srcy = np.array([item[1] for item in src_pair])


    print('reduce chi-square: ', chi_square_map.sum()/chi_square_map.shape[0]**2)

    cbpar = {}
    #cbpar['pad'] = 0.02
    cbpar['shrink'] = 0.5
    default_cmap=plt.get_cmap('jet')
    current_cmap=default_cmap
    myargs = {'origin':'lower'}
    myargs['cmap'] = current_cmap
    ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]
    myargs['extent'] = ext

    plt.figure()
    percent = [0,100]
    plt.subplot(2,2,1)
    vmin = np.percentile(fitter.ccd_image,percent[0]) 
    vmax = np.percentile(fitter.ccd_image,percent[1]) 
    plt.imshow(fitter.ccd_image,vmin=vmin,vmax=vmax,**myargs)
    if positions is not None:
        plt.plot(posx,posy,'*',color='k')
    cb=plt.colorbar(**cbpar)
    cb.ax.minorticks_on()
    cb.ax.tick_params(labelsize='small')
    plt.title('Data')
    plt.xlabel('Arcsec')
    plt.ylabel('Arcsec')

    plt.subplot(2,2,2)
    vmin = np.percentile(best_image,percent[0]) 
    vmax = np.percentile(best_image,percent[1]) 
    plt.imshow(best_image,vmin=vmin,vmax=vmax,**myargs)
    cb=plt.colorbar(**cbpar)
    cb.ax.minorticks_on()
    cb.ax.tick_params(labelsize='small')
    plt.title('Lens reconstruction')
    plt.xlabel('Arcsec')
    plt.ylabel('Arcsec')

    plt.subplot(2,2,3)
    vmin = np.percentile(chi_square_map,percent[0]) 
    vmax = np.percentile(chi_square_map,percent[1]) 
    plt.imshow(chi_square_map,vmin=vmin,vmax=vmax,**myargs)
    cb=plt.colorbar(**cbpar)
    cb.ax.minorticks_on()
    cb.ax.tick_params(labelsize='small')
    plt.title('Chi-square map')
    plt.xlabel('Arcsec')
    plt.ylabel('Arcsec')

    criticals,caustics = get_critical_line_and_caustic(thislens_mass,highresbox=[-2.,2.,-2.,2.],
                                                numres=0.001)
    plt.subplot(2,2,4)
    myargs['extent'] = [sgridx.min(),sgridx.max(),sgridx.min(),sgridx.max()]
    #cs = plt.contour(sgridx,sgridy,I_source,colors='k')
    #plt.clabel(cs,inline=1, fontsize=10)
    vmin = np.percentile(I_source,percent[0]) 
    vmax = np.percentile(I_source,percent[1]) 
    plt.imshow(I_source,vmin=vmin,vmax=vmax,**myargs)
    cb=plt.colorbar(**cbpar)
    cb.ax.minorticks_on()
    cb.ax.tick_params(labelsize='small')
    plt.scatter(xsrc,ysrc,s=0.1,color='k')
    if positions is not None:
        plt.plot(srcx,srcy,'*',color='k')

    for caustic in caustics:
        plt.plot(caustic[:,0],caustic[:,1],'k-')
        
    plt.title('Chi-square map')
    plt.xlabel('Arcsec')
    plt.ylabel('Arcsec')

    plt.tight_layout()
    plt.savefig('./output.png',dpi=200)
    plt.show()

    #util.matplot_show(alphax,ext)
    #util.matplot_show(alphay,ext)
    #for item in tracer.lens_mass:
    #    item._altered = True
    #print('xxxx',tracer.total_deflection(-0.15, -1.2))