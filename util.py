import numpy as np
from astropy.io import fits
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import copy 
#from RayTracing import Tracer
INF_CHI2 = np.inf

def phi_gamma_ellipticity(phi, gamma):
    """
    :param phi: angel
    :param gamma: ellipticity
    :return:
    """
    e1 = gamma*np.cos(2*phi*np.pi/180.)
    e2 = gamma*np.sin(2*phi*np.pi/180.)
    return e1, e2


def ellipticity2phi_gamma(e1, e2):
    """
    :param e1: ellipticity component
    :param e2: ellipticity component
    :return: angle and abs value of ellipticity
    """
    phi = np.arctan2(e2, e1)/2*180./np.pi
    gamma = np.sqrt(e1**2+e2**2)
    if phi < 0 :
        return phi+180.,gamma
    else:
        return phi,gamma          

def change_q_theta_to_e1e2(q,theta):
    """
    :param q: axis ratio
    :param theta: position angle
    :return: two ellipticity component
    """
    theta *= np.pi/180.
    fac = (1-q)/(1+q)
    e1 = fac*np.cos(2*theta)
    e2 = fac*np.sin(2*theta)
    return e1,e2

def change_e1e2_to_q_theta(e1,e2):
    """
    :param e1: ellipticity component
    :param e2: ellipticity component
    :return: axis ratio and position angle
    """
    theta = np.arctan2(e2,e1)/2
    theta *= 180./np.pi
    fac = np.sqrt(e1**2+e2**2)
    if fac > 0.999:
        fac = 0.999  #avoid unphysical solution 
    #if fac > 1: print('unphysical e1,e2')
    q = (1-fac)/(1+fac)
    return q,theta

#@jit(nopython=True)
def xy_transform(x, y, x_cen, y_cen, phi):
    """
    A rotational matrix exert on 2-d vector
    :param x: the x coordinate of the vector, which need to be rotated 
    :param y: the y coordinate of the vector, which need to be rotated  
    :param x_cen: the x coordinate of rotation center
    :param y_cen: the y coordinate of rotation center
    :param phi: the rotation angle in degree
    x,y can be a scalar or array. suppose x,y is a scalar, then this
    matrix rotate the 2-d vector (x,y) clockwisely with respect to the
    center (x_cen,y_cen).
    """
    xnew=(x-x_cen)*np.cos(np.pi*phi/180.0)+(y-y_cen)*np.sin(np.pi*phi/180.0)
    ynew=-(x-x_cen)*np.sin(np.pi*phi/180.0)+(y-y_cen)*np.cos(np.pi*phi/180.0)
    return (xnew, ynew)


#load data util; image,psf,noise
def load_data(image_file=None,noise_file=None,weight_file=None,psf_file=None, mask_file=None ,pix_size=None):
    if pix_size is None:
        raise Exception('You need to set pixel size in arcsecs')
    image = fits.open(image_file)[0].data
    psf = fits.open(psf_file)[0].data
    if weight_file is not None:
        weight = fits.open(weight_file)[0].data
        with np.errstate(all='ignore'):  # Division by zero fixed via isnan
            noise = np.copy(1/weight)
            id_nan = np.isnan(noise)
            noise[id_nan] = 1e8
    elif noise_file is not None:
        noise = fits.open(noise_file)[0].data
        weight = 1/noise**2
    else:
        raise Exception('No noise/weight map')
    if mask_file is not None:
        mask = fits.open(mask_file)[0].data
    else:
        mask = None
    
    return {'image':image,'noise':noise,'psf':psf,'dpix':pix_size,'weight':weight,'mask':mask}
    

def load_data_from_biz(file_name=None,pix_size=None,sub_grid=None):
    if pix_size is None:
        raise Exception('You need to set pixel size in arcsecs')
    image = fits.getdata(file_name,header=False,ext=0)
    weight = fits.getdata(file_name,header=False,ext=1)
    psf = fits.getdata(file_name,header=False,ext=3)
    #TODO: load mask. use gui mask utility
    # more things about mask
    mask = None
    return {'image':image,'weight':weight,'psf':psf,'dpix':pix_size, 'sub_grid':sub_grid,'mask':mask}  


def generate_mask(x,y,radii):
    """
    x,y: 2-d image grid
    radii: the circular mask radii
    """
    r = np.sqrt(x**2 + y**2)
    return r>radii


def plot_image(image=None,x=None,y=None,figure=None):
    """
    image: the 2-d array you want visulize
    figure: A matplotlib figure object
    x,y: image grid in arcsecs or physical unit
    """
    if figure is None:
        figure = plt.figure()

    if (x is None) or (y is None):
        raise Exception('You need to give grid in arcsecs, to visualize image in arcsec/physical units')

    
    #dpix = np.abs(x[0,1] - x[0,0])
    ext = [x.min(),x.max(),y.min(),y.max()]
    keywords = {'origin':'lower','cmap':'jet','extent':ext}

    plt.figure(figure.number)
    plt.imshow(image,**keywords)
    plt.colorbar()
    plt.xlabel('x [arcsecs]')
    plt.ylabel('y [arcsecs]')
    return figure


def cut_image_around_center(image,hw):
    n1,n2 = image.shape
    c1 = int(n1/2)
    c2 = int(n2/2)
    im = np.copy(image)
    return im[c1-hw:c1+hw,c2-hw:c2+hw]  #even num of grid, should I hw+2?

def generate_grid(hw,dpix,sub_grid=None):
    coord = np.arange(-hw,hw,1)*dpix + dpix*0.5 #!!!!!!! offset half pix
    x,y = np.meshgrid(coord,coord)

    if sub_grid is None:
        return x,y,None,None 
    else:
        hw1 = hw*sub_grid
        dpix1 = 1.0*dpix/sub_grid
        coord1 = np.arange(-hw1,hw1,1)*dpix1 + dpix1*0.5
        xsub,ysub = np.meshgrid(coord1,coord1)
        return x,y,xsub,ysub


def bin_image(arr, sub_grid=None):
    new_shape = (arr.shape[0]//sub_grid,arr.shape[1]//sub_grid)
    shape = (new_shape[0], sub_grid,
             new_shape[1], sub_grid)
    return arr.reshape(shape).mean(axis=(-1,1))

def bin_index(arr, sub_grid=None):
    new_shape = (arr.shape[0]//sub_grid,arr.shape[1]//sub_grid)
    shape = (new_shape[0], sub_grid,
             new_shape[1], sub_grid)
    return arr.reshape(shape)

def generate_submask_from_mask(mask,subshape,sub_grid=None):
    mask_sub = np.ones(subshape)
    idd = np.arange(subshape[0]*subshape[1])
    idd = idd.reshape(subshape)
    bin_idd = bin_index(idd, sub_grid=sub_grid)
    n1,n2 = mask.shape
    for i in range(n1):
        for j in range(n2):
            sub_id_element = bin_idd[i, :, j, :].reshape(-1)
            if mask[i,j] == 0.0:
                for idx in sub_id_element:
                    mask_sub[np.unravel_index(idx,subshape)] = 0.0
    return mask_sub

def matplot_show(im,ext=None):
    plt.figure()
    plt.imshow(im,origin='lower',extent=ext,cmap='jet')
    plt.colorbar()
    plt.show() 

def matplot_show_fig(im,ext=None):
    fig = plt.figure()
    plt.imshow(im,origin='lower',extent=ext,cmap='jet')
    plt.colorbar()
    return fig


def get_position_from_image(image, extent=None):
    def onclick(event):
        if event.dblclick:
           print('%s click: button=%d, [%.4f, %.4f]' %
                ('double', event.button,
                event.xdata,
                event.ydata))
    fig, ax = plt.subplots()  
    im = ax.imshow(image,cmap='jet',extent=extent,origin='lower')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im, cax=cax)
    ax.set_title('Get positions by double click')
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    fig.canvas.mpl_disconnect(cid)
    plt.close(fig)

