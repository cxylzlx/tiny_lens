import numpy as np
import pygmo as pg 
from pso_lbest import ParticleSwarmOptimizer 
from scipy.stats import norm
import pymultinest
import json
import dynesty
import dynesty.utils as dyfunc
from modelling import AbstractFitRoutine,AbstractFitRoutine_1d


#---------------------------------1D---New-----------------------------------
#----------------genetic fitting-----------------------        
class GeneticFitRoutine_1d(AbstractFitRoutine_1d):

    def fitness(self,params):
        retval = np.zeros((1,))
        retval[0] = 0.5*self.chi2(params)
        return retval

    def get_bounds(self):
        return (np.array(self.prior_0),np.array(self.prior_1))


#----------------pso fitting----------------------- 
class PSOFitRoutine_1d(AbstractFitRoutine_1d):
    #@profile
    def fitness(self,params):
        return -0.5*self.chi2(params),None

    def run(self):
        pso = ParticleSwarmOptimizer(self.fitness, self.prior_0, self.prior_1,particleCount=49,threads=5)
        pso.optimize(maxIter=2000,m=10**(-7))
        self.best_fit = pso.gbest.position

#------------------pymultinest----------------------
class PymultinestFitRoutine_1d(AbstractFitRoutine_1d):

    def loglike(self, cube ,ndim, nparams):
        return  -0.5*self.chi2(cube)
        
    def prior(self, cube, ndim, nparams):
        for i in range(ndim):
            if self.prior_type[i]==0: #uniform
                cube[i] = (self.prior_1[i]-self.prior_0[i])*cube[i]+self.prior_0[i]
            elif self.prior_type[i]==1: #gaussian
                cube[i] = norm.ppf(cube[i],loc=self.prior_0[i],scale=self.prior_1[i])
            else: #log-uniform
                cube[i] = 10**((np.log10(self.prior_1[i])-np.log10(self.prior_0[i]))*cube[i]+np.log10(self.prior_0[i]))

    def run(
        self, 
        resume=False,
        verbose = True,
        outputfiles_basename='output_multinest',
        n_live_points=80,
        evidence_tolerance=0.5,
        sampling_efficiency=0.3,
        importance_nested_sampling=False,
        const_efficiency_mode=True,
        multimodal=True
        ):
        pymultinest.run(self.loglike, self.prior, self.ndim, resume =resume, verbose=verbose,outputfiles_basename=outputfiles_basename+'/',
                   n_live_points=n_live_points, evidence_tolerance=evidence_tolerance,sampling_efficiency=sampling_efficiency,
                   importance_nested_sampling=importance_nested_sampling,const_efficiency_mode=const_efficiency_mode, multimodal=multimodal)
        json.dump(self.params_name, open('{}/params.json'.format(outputfiles_basename), 'w'))
        res = pymultinest.Analyzer(outputfiles_basename='{}/'.format(outputfiles_basename), n_params = self.ndim)
        self.best_fit = res.get_best_fit()['parameters']
#---------------------------------1D--------------------------------------



#---------------------------------2D------------------------------------
#----------------genetic fitting-----------------------        
class GeneticFitRoutine(AbstractFitRoutine):

    def fitness(self,params):
        retval = np.zeros((1,))
        retval[0] = 0.5*self.chi2(params)
        return retval

    def get_bounds(self):
        return (np.array(self.prior_0),np.array(self.prior_1))


#----------------pso fitting----------------------- 
class PSOFitRoutine(AbstractFitRoutine):
    #@profile
    def fitness(self,params):
        return -0.5*self.chi2(params),None

    def run(self):
        pso = ParticleSwarmOptimizer(self.fitness, self.prior_0, self.prior_1,particleCount=49,threads=5)
        pso.optimize(maxIter=2000,m=10**(-7))
        self.best_fit = pso.gbest.position

#------------------pymultinest----------------------
class PymultinestFitRoutine(AbstractFitRoutine):

    def loglike(self, cube ,ndim, nparams):
        return  -0.5*self.chi2(cube)
        
    def prior(self, cube, ndim, nparams):
        for i in range(ndim):
            if self.prior_type[i]==0: #uniform
                cube[i] = (self.prior_1[i]-self.prior_0[i])*cube[i]+self.prior_0[i]
            elif self.prior_type[i]==1: #gaussian
                cube[i] = norm.ppf(cube[i],loc=self.prior_0[i],scale=self.prior_1[i])
            else: #log-uniform
                cube[i] = 10**((np.log10(self.prior_1[i])-np.log10(self.prior_0[i]))*cube[i]+np.log10(self.prior_0[i]))


    def run(
        self, 
        resume=False,
        verbose = True,
        outputfiles_basename='output_multinest',
        n_live_points=80,
        evidence_tolerance=0.5,
        sampling_efficiency=0.3,
        importance_nested_sampling=False,
        const_efficiency_mode=True,
        multimodal=True
        ):
        pymultinest.run(self.loglike, self.prior, self.ndim, resume =resume, verbose=verbose,outputfiles_basename=outputfiles_basename+'/',
                   n_live_points=n_live_points, evidence_tolerance=evidence_tolerance,sampling_efficiency=sampling_efficiency,
                   importance_nested_sampling=importance_nested_sampling,const_efficiency_mode=const_efficiency_mode, multimodal=multimodal)
        json.dump(self.params_name, open('{}/params.json'.format(outputfiles_basename), 'w'))
        res = pymultinest.Analyzer(outputfiles_basename='{}/'.format(outputfiles_basename), n_params = self.ndim)
        self.best_fit = res.get_best_fit()['parameters']
#---------------------------------2D------------------------------------