from parameter import Paramter
import util
import numpy as np

class Sersic(object):
    """
    Class to hold parameters of an elliptical Sersic light profile, ie
    I(x,y) = A * exp(-bn*((r/reff)^(1/n)-1)),
    where bn makes reff enclose half the light (varies with Sersic index), 
    This profile is parameterized by the major axis and axis ratio; you can get the half-light
    radius with r_eff = majax * sqrt(axisratio).
    
    Parameters:
    xc, yc
        Position of the source in arcseconds relative to the image center. +x is west, +y is north.
    I_eff
        The intensity at effective radius(half light radius)
    r_eff
        The effective radius in inermediate-axis convention
    n
        The Sersic profile index n (0.5 is ~Gaussian, 1 is ~an exponential disk, 4
        is a de Vaucoleurs profile). 
    q
        axis ratio
    PA
        Source position angle. this is in degreee CCW with positive x direction
    """

    def __init__(self,xc=None,yc=None,I_eff=None,r_eff=None,n=None,q=None,PA=None,e1=None,e2=None):

        # Do some input handling.
        if not isinstance(xc,Paramter):
            xc = Paramter(value=xc,low_lim=-10.0,up_lim=10.0)
        if not isinstance(yc,Paramter):
            yc = Paramter(value=yc,low_lim=-10.0,up_lim=10.0)
        if not isinstance(I_eff,Paramter):
            I_eff = Paramter(value=I_eff,low_lim=0.0,up_lim=100.0)
        if not isinstance(r_eff,Paramter):
            r_eff = Paramter(value=r_eff,low_lim=0.0,up_lim=10.0)
        if not isinstance(n,Paramter):
            n = Paramter(value=n,low_lim=0.3,up_lim=8.0)
        if not isinstance(q,Paramter):
            q = Paramter(value=q,low_lim=0.0,up_lim=1.0)
        if not isinstance(PA,Paramter):
            PA = Paramter(value=PA,low_lim=0.0,up_lim=180.0)
        if not isinstance(e1,Paramter):
            e1 = Paramter(value=e1,low_lim=-0.5,up_lim=0.5)
        if not isinstance(e2,Paramter):
            e2 = Paramter(value=e2,low_lim=-0.5,up_lim=0.5)

        self.xc = xc
        self.yc = yc
        self.I_eff = I_eff
        self.r_eff = r_eff
        self.n = n
        self.q = q
        self.PA = PA
        self.e1 = e1
        self.e2 = e2

        #either set PA & e, or set e1 & e2 
        if ((self.q.value is None) and (self.PA.value is None)):
            self.update_q_theta_from_e1e2()

        if ((self.e1.value is None) and (self.e2.value is None)):
            self.update_e1e2_from_q_theta()

    def update_e1e2_from_q_theta(self):
        """
        when we update e and theta parameter, we use this function 
        to update correponding e1/e2 simultaniously  
        """
        self.e1.value,self.e2.value = util.change_q_theta_to_e1e2(self.q.value,self.PA.value)
          
    def update_q_theta_from_e1e2(self):
        """
        when we update e1 and e2 parameter, we use this function 
        to update correponding e/theta simultaniously  
        """
        self.q.value,self.PA.value = util.change_e1e2_to_q_theta(self.e1.value,self.e2.value)

    #@profile
    def get_instensity(self,xsource,ysource):
        """
        note the position angle in the sersic_2d is off by 90 degrees compared to sersic_phot
        spar[0]:amplitude  [1]:xcen  [2]:ycen  [3]:sigma  [4]:e1  [5]:e2  [6]:sersic index
        spar[0]:amplitude  [1]:xcen  [2]:ycen  [3]:sigma  [4]:postion angle  [5]:q--axis ratio [6]:sersic index
        """
        #xsource, ysource = np.copy(xsource), np.copy(ysource) # for safety. #seems we don't chane xsource, so we don't need copy.

        if (self.e1.value is not None) and (self.e2.value is not None):
            self.update_q_theta_from_e1e2()

        (xnew, ynew)=util.xy_transform(xsource, ysource, self.xc.value, self.yc.value, self.PA.value)
        n = self.n.value
        if n >= 0.36: # from Ciotti & Bertin 1999, truncated to n^-3
            k=2.0*n-1./3+4./(405.*n)+46./(25515.*n**2.)+131./(1148175.*n**3.)
        else: # from MacArthur et al. 2003
            k=0.01945-0.8902*n+10.95*n**2.-19.67*n**3.+13.43*n**4.
        r=np.sqrt(self.q.value*xnew**2.+ynew**2./self.q.value)
        return self.I_eff.value*np.exp(-k* ((r/self.r_eff.value)**(1./n)-1))

