import copy
import util

class Abstractdataloading(object):
    """
    An Abstract data loading class
    Include below components:
    1. accept observational data
    2. accept the lens/source model used for modelling
    """

    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0,hw=None):
        """
        data_dict: the observation data dict 
        lens_mass: lens mass model list 
        lens_light: lens light model list
        source: source model list
        positions: The position pairs are required to tracing back to a small region in source-plane
        hw: the half-width of image. used for cutting image to improve the modelling speed
        """
        self.ccd_image = data_dict['image']
        self.weight = data_dict['weight']
        self.psf = data_dict['psf']
        self.dpix = data_dict['dpix']
        self.sub_grid = data_dict['sub_grid']
        self.lens_mass = copy.deepcopy(lens_mass)
        self.lens_light = copy.deepcopy(lens_light)
        self.source_light = copy.deepcopy(source_light)
        self.ndim = 0
        self.params_init = []
        self.params_name = []
        ### TODO: change hw to image size
        if hw is None:
            self.hw = int(self.ccd_image.shape[0]/2) #half image size
            self.ccd_image = util.cut_image_around_center(self.ccd_image,self.hw)
            self.weight = util.cut_image_around_center(self.weight,self.hw)
        else:
            self.hw = hw
            self.ccd_image = util.cut_image_around_center(self.ccd_image,self.hw)
            self.weight = util.cut_image_around_center(self.weight,self.hw)

        self.x, self.y, self.xsub, self.ysub = util.generate_grid(self.hw,self.dpix,sub_grid=self.sub_grid)
        
        self.positions = positions
        self.threshhold = threshhold
        self.prior_type = []
        self.prior_0 = []
        self.prior_1 = []

