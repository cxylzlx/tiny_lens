'''
Created on Sep 30, 2013

@author: J. Akeret
'''
from __future__ import print_function, division, absolute_import, unicode_literals
from copy import copy
from math import floor
import math
import multiprocessing
import numpy
import warnings
import sys

warnings.filterwarnings('ignore',"Using a non-tuple sequence")
warnings.filterwarnings('error',"invalid value encountered in double_scalars")
class ParticleSwarmOptimizer(object):
    '''
    Optimizer using a swarm of particles

    :param func:
        A function that takes a vector in the parameter space as input and
        returns the natural logarithm of the posterior probability for that
        position.

    :param low: array of the lower bound of the parameter space
    :param high: array of the upper bound of the parameter space
    :param particleCount: the number of particles to use.
    :param threads: (optional)
        The number of threads to use for parallelization. If ``threads == 1``,
        then the ``multiprocessing`` module is not used but if
        ``threads > 1``, then a ``Pool`` object is created and calls to
        ``lnpostfn`` are run in parallel.

    :param pool: (optional)
        An alternative method of using the parallelized algorithm. If
        provided, the value of ``threads`` is ignored and the
        object provided by ``pool`` is used for all parallelization. It
        can be any object with a ``map`` method that follows the same
        calling sequence as the built-in ``map`` function.

    '''


    def __init__(self, func, low, high, particleCount=25, threads=1, pool=None,init_guess=None):
        '''
        Constructor
        '''
        self.func = func
        self.low = numpy.asarray(low)
        self.high = numpy.asarray(high)
        self.particleCount = particleCount
        self.threads = threads
        self.pool = pool
        
        if self.threads > 1 and self.pool is None:
            self.pool = multiprocessing.Pool(self.threads)

        self.paramCount = len(self.low)

        self.swarm = self._initSwarm(init_guess=init_guess)
        self.gbest = Particle.create(self.paramCount)

        self.bestfit_iter=0.0
        self.bestfit = -numpy.inf
        self.converge_count=100

    def _initSwarm(self,init_guess=None):
        #implement half diff velocity initialize method
        if init_guess is None:
            swarm = []
            for _ in range(self.particleCount):
                pos = numpy.random.uniform(self.low, self.high, size=self.paramCount)
                init_vel = (numpy.random.uniform(self.low, self.high, size=self.paramCount) - pos)/2.0
                swarm.append(Particle(pos, init_vel))
            return swarm
        else:
            swarm = []
            print('initial guess----------:',init_guess)
            print('inital value likelihood------: ',self.func(init_guess)[0])
            vel_p0 = numpy.random.uniform(self.low, self.high, size=self.paramCount) - init_guess
            swarm.append(Particle(init_guess,vel_p0))
            for _ in range(self.particleCount-1):
                pos = numpy.random.uniform(self.low, self.high, size=self.paramCount)
                init_vel = (numpy.random.uniform(self.low, self.high, size=self.paramCount) - pos)/2.0
                swarm.append(Particle(pos, init_vel))
            return swarm

            

    def sample(self, maxIter=1000, c1=1.193, c2=1.193, p=0.7, m=10**-3, n=10**-2, minstep=100, tol=1e-4,niter=10,clp=[0.0,1.0]):
        """
        Launches the PSO. Yields the complete swarm per iteration

        :param maxIter: maximum iterations
        :param c1: cognitive weight
        :param c2: social weight
        :param p: stop criterion, percentage of particles to use
        :param m: stop criterion, difference between mean fitness and global best
        :param n: stop criterion, difference between norm of the particle vector and norm of the global best
        :param niter: pdate velocity length each niter step
        """
        self._get_fitness(self.swarm)
        i = 0

        #below blcok is for adaptive velocity Initialization
        #vel_length = numpy.sum((self.high - self.low)/2.)/self.high.size  #initial vel step
        #succes_count = 0
        #print('initial vel length----:',vel_length)

        #blow blcok is for velocity clamping
        prange = (self.high - self.low) #/2.
        vel_low = prange*clp[0]
        print('velocity clamping low:',vel_low)
        vel_high = prange*clp[1]
        print('velocity clamping high:',vel_high)
        

        while True:

            #update personal/local best information---------
            for index,particle in enumerate(self.swarm):
                #save the global best solution 
                if ((self.gbest.fitness)<particle.fitness):
                    self.gbest = particle.copy()
                    if numpy.abs(self.bestfit-self.gbest.fitness)>1e-4:
                        self.bestfit = self.gbest.fitness
                        self.bestfit_iter = i
            
                #update local best value of each particle in swarms
                #print('The index of particle:',index,'return neighbour index:',self._get_neighbour_index(index))
                neighbour_particle = [self.swarm[itmp].pbest for itmp in self._get_neighbour_index(index)]
                fitness = numpy.array([part.fitness for part in neighbour_particle])
                id_tmp1 = fitness.argsort()[::-1]
                sort_fitness = numpy.ediff1d(fitness[id_tmp1])
                toltmp = tol
                if numpy.abs(sort_fitness[0]) < toltmp:
                    if numpy.abs(sort_fitness[1]) < toltmp:
                        if numpy.abs(sort_fitness[2]) < toltmp:
                            id_tmp2 = numpy.random.choice(id_tmp1,1)[0]
                        else:
                            id_tmp2 = numpy.random.choice(id_tmp1[0:3],1)[0]
                    else:
                        id_tmp2 = numpy.random.choice(id_tmp1[0:2],1)[0]
                else:
                    id_tmp2 = id_tmp1[0]
                particle.lbest = neighbour_particle[id_tmp2].copy()
                #print('lbest fitness:',fitness)

                #update personal best of each particle in swarms
                if (particle.fitness > particle.pbest.fitness):
                    particle.updatePBest()
                #    succes_count += 1
                try:
                    if ( (numpy.abs(particle.fitness - particle.pbest.fitness)<tol) and (numpy.random.random()<0.5) ):
                        particle.updatePBest()
                #        succes_count += 1
                except Warning:
                    print('warning')
            
            #if ((i+1) % niter) ==0 :
            #    succes_rate = succes_count/(self.paramCount*niter)
            #    if succes_rate > 0.2: #1/5 rule 
            #        vel_length *= 2.0
            #    else:
            #        vel_length /= 2.0
            #    succes_count = 0
            #    print('updated velocity length--------:',vel_length)
            #    print('current position of particle:',self.gbest.position)

            if(i>=maxIter):
                print("max iteration reached! stoping")
                return

            if(self._converged(i)):
                if(self.isMaster()):
                    print("converged after %s iterations!"%i)
                    print("best fit found: ", self.gbest.fitness, self.gbest.position)
                return

            for particle in self.swarm:

                w = 0.5 + numpy.random.uniform(0,1,size=self.paramCount)/2
                #w=0.72984
                #w2 = 1.496172
                #w = 1.8
                part_vel = w * particle.velocity
                cog_vel = c1 * numpy.random.uniform(0,1,size=self.paramCount) * (particle.pbest.position - particle.position)
                soc_vel = c2 * numpy.random.uniform(0,1,size=self.paramCount) * (particle.lbest.position - particle.position)
                particle.velocity = part_vel + cog_vel + soc_vel
                #fac = vel_length/numpy.linalg.norm(particle.velocity)
                #particle.velocity *= fac 

                particle.velocity = numpy.where(numpy.abs(particle.velocity)>=vel_high,numpy.sign(particle.velocity)*vel_high,particle.velocity)
                #assert  ~numpy.any((numpy.abs(particle.velocity - xx) > 1e-3))
                if (clp[0] >= 1e-6):
                    particle.velocity = numpy.where(numpy.abs(particle.velocity)<=vel_low,numpy.sign(particle.velocity)*vel_low,particle.velocity)

                particle.position = particle.position + particle.velocity

                #infinite fitness bound
                #set the infeasible pos particle's fitness to infifite value
                #id_underflow = (particle.position > self.low)
                #id_overflow = (particle.position < self.high)
                #particle.bounded = numpy.all(numpy.logical_and(id_underflow,id_overflow)) #set the bounded condition atrribute
                #if not particle.bounded :
                #    particle.fitness = -numpy.inf

                id_underflow = (particle.position > self.low)
                id_overflow = (particle.position < self.high)
                particle.bounded = numpy.all(numpy.logical_and(id_underflow,id_overflow)) #set the bounded condition atrribute
                id_out_of_lower = numpy.where(~id_underflow)
                id_out_of_upper = numpy.where(~id_overflow)
                #if id_lower[0].size != 0:
                if ~particle.bounded:
                    #print('The out of high/lower bound id:',id_out_of_upper,id_out_of_lower)
                    #particle.position[id_out_of_lower] = self.low[id_out_of_lower] 
                    particle.position[id_out_of_lower] = 2*self.low[id_out_of_lower] - particle.position[id_out_of_lower]
                    particle.velocity[id_out_of_lower] *= -1.0*numpy.random.uniform(0,1,size=id_out_of_lower[0].size) 
                    #particle.position[id_out_of_upper] = self.high[id_out_of_upper] 
                    particle.position[id_out_of_upper] = 2*self.high[id_out_of_upper] - particle.position[id_out_of_upper]
                    particle.velocity[id_out_of_upper] *= -1.0*numpy.random.uniform(0,1,size=id_out_of_upper[0].size) 


            self._get_fitness(self.swarm)

            swarm = []
            for particle in self.swarm:
                swarm.append(particle.copy())
            yield swarm

            if(i%10==0):
                print('Number of PSO iteration:',i+1,' Best Fitness:',self.gbest.fitness)
                print('Number of PSO iteration:',i+1,' Best pos:',self.gbest.position)
                
            i+=1

    def optimize(self, maxIter=1000, c1=1.193, c2=1.193, p=0.7, m=10**-3, n=10**-2,minstep=100,tol=1e-4,niter=50,clp=[0.0,1.0]):
        """
        Runs the complete optimiziation.

        :param maxIter: maximum iterations
        :param c1: cognitive weight
        :param c2: social weight
        :param p: stop criterion, percentage of particles to use
        :param m: stop criterion, difference between mean fitness and global best
        :param n: stop criterion, difference between norm of the particle vector and norm of the global best

        :return swarms, gBests: the swarms and the global bests of all iterations
        """

        swarms = []
        gBests = []
        for swarm in self.sample(maxIter,c1,c2,p,m,n,minstep,tol,niter,clp=clp):
            swarms.append(swarm)
            gBests.append(self.gbest.copy())

        return swarms, gBests
    
 #   @staticmethod
 #   def _dec_func(pos):
 #       if self.bounded:
 #           return self.func(*args)
 #       else:
 #           return -1e8
#
#    def _get_fitness(self,swarm):0,1,size=self.paramCount
#
#        # If the `pool` property of the pso has been set (i.e. we want
#        # to use `multiprocessing`), use the `pool`'s map method. Otherwise,
#        # just use the built-in `map` function.
#        if self.pool is not None:
#            mapFunction = self.pool.map
#        else:
#            mapFunction = map
#        #pos include the position info of all bounded particle(i.e. within boundary)
#        pos = numpy.array([part.position for part in swarm if part.bounded])
#        results =  mapFunction(self.func, pos)
#        lnprob = numpy.array([l[0] for l in results])
#        i=0
#        for particle in swarm:
#            if particle.bounded:
#                particle.fitness = lnprob[i]
#                i += 1

    def _get_fitness(self,swarm):

        # If the `pool` property of the pso has been set (i.e. we want
        # to use `multiprocessing`), use the `pool`'s map method. Otherwise,
        # just use the built-in `map` function.
        if self.pool is not None:
            mapFunction = self.pool.map
        else:
            mapFunction = map

        pos = numpy.array([part.position for part in swarm])
        results =  mapFunction(self.func, pos)
        lnprob = numpy.array([l[0] for l in results])
        for i,particle in enumerate(swarm):
            particle.fitness = lnprob[i]

    def _get_neighbour_index(self,index):
        """
        Von-neumann topology index
        """
        sqrtn = int(numpy.sqrt(self.particleCount))
        if sqrtn**2 != self.particleCount:
            raise Exception("Number of particles needs to be perfect " +
                                "square if using Von Neumann neighbourhood " +
                                "topologies.")
        neighbour_index = [index,(index - sqrtn) % self.particleCount, #upper neighbour
                                 (index + sqrtn) % self.particleCount, #lower neighbour
                            ((index // sqrtn) * sqrtn) + ((index + 1) % sqrtn), #right neighbour
                            ((index // sqrtn) * sqrtn) + ((index - 1) % sqrtn)] #left neighbour
        return neighbour_index
        
    def _converged(self,iter):
        if (iter-self.bestfit_iter)>self.converge_count:
            return True

#    def _converged(self, it, p, m, n):
#        fit = self._convergedFit(it=it, p=p, m=m)
#        if fit:
#            space = self._convergedSpace(it=it, p=p, m=n)
#            return space
#        else:
#            return False

    def _convergedFit(self, it, p, m):
        bestSort = numpy.sort([particle.pbest.fitness for particle in self.swarm])[::-1]
        meanFit = numpy.mean(bestSort[1:int(math.floor(self.particleCount*p))])
#        print( "best %f, meanFit %f, ration %f"%( self.gbest[0], meanFit, abs((self.gbest[0]-meanFit))))
        return abs(self.gbest.fitness-meanFit) < m

    def _convergedSpace(self, it, p, m):
        sortedSwarm = [particle for particle in self.swarm]
        sortedSwarm.sort(key=lambda part: -part.fitness)
        bestOfBest = sortedSwarm[0:int(floor(self.particleCount*p))]

        diffs = []
        for particle in bestOfBest:
            diffs.append(self.gbest.position-particle.position)

        maxNorm = max(list(map(numpy.linalg.norm, diffs)))
        return (abs(maxNorm)<m)

    def _convergedSpace2(self, p):
        #Andres N. Ruiz et al.
        sortedSwarm = [particle for particle in self.swarm]
        sortedSwarm.sort(key=lambda part: -part.fitness)
        bestOfBest = sortedSwarm[0:int(floor(self.particleCount*p))]

        positions = [particle.position for particle in bestOfBest]
        means = numpy.mean(positions, axis=0)
        delta = numpy.mean((means-self.gbest.position)/self.gbest.position)
        return numpy.log10(delta) < -3.0

    def isMaster(self):
        return True

class Particle(object):
    """
    Implementation of a single particle

    :param position: the position of the particle in the parameter space
    :param velocity: the velocity of the particle
    :param fitness: the current fitness of the particle

    """

    def __init__(self, position, velocity, fitness = 0):
        self.position = position
        self.velocity = velocity

        self.fitness = fitness
        self.paramCount = len(self.position)
        self.pbest = self
        self.lbest = self
        self.bounded = True #whether the particle is within boundary, default is true

    @classmethod
    def create(cls, paramCount):
        """
        Creates a new particle without position, velocity and -inf as fitness
        """

        return Particle(numpy.array([[]]*paramCount),numpy.array([[]]*paramCount),-numpy.Inf)

    def updatePBest(self):
        """
        Sets the current particle representation as personal best
        """
        self.pbest = self.copy()

    def copy(self):
        """
        Creates a copy of itself
        """
        return Particle(copy(self.position),
                        copy(self.velocity),
                        self.fitness)

    def __str__(self):
        return "%f, pos: %s velo: %s"%(self.fitness, self.position, self.velocity)

    def __unicode__(self):
        return self.__str__()
