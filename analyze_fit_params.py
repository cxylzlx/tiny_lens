import copy
from data_loading import Abstractdataloading

class AnalyzeFitParams(Abstractdataloading):
    def __init__(self,data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0,hw=None):
        super(AnalyzeFitParams,self).__init__(
            data_dict=data_dict,
            lens_mass=lens_mass,
            lens_light=lens_light,
            source_light=source_light,
            positions=positions,
            threshhold=threshhold,
            hw=hw
        )

    def pass_math_relation(self,thislens_mass,thislens_light,thissource_light):
        '''
        if you set a relation like a=b+1,you should set parameter <a> value
        to fixed at the initialization step
        '''
        pass

    def get_params_from_model(self):
        #check lens mass
        for i,ilens in enumerate(self.lens_mass):
            if ilens.__class__.__name__=='SIELens':
                for key in ['xc','yc','b_sie','e1','e2']:
                    if not vars(ilens)[key].fixed:
                        self.ndim += 1
                        self.params_init.append(vars(ilens)[key].value)
                        self.params_name.append(key+'_lens_mass'+str(i))
                        self.prior_type.append(vars(ilens)[key].prior_type)
                        self.prior_0.append(vars(ilens)[key].prior_0)
                        self.prior_1.append(vars(ilens)[key].prior_1)
        
            elif ilens.__class__.__name__=='ExternalShear':
                for key in ['e1','e2']:
                    if not vars(ilens)[key].fixed:
                        self.ndim += 1
                        self.params_init.append(vars(ilens)[key].value)
                        self.params_name.append(key+'_shear_lens_mass')
                        self.prior_type.append(vars(ilens)[key].prior_type)
                        self.prior_0.append(vars(ilens)[key].prior_0)
                        self.prior_1.append(vars(ilens)[key].prior_1)
        
        #check lens light
        if self.lens_light is not None:
            for i,ilens in enumerate(self.lens_light):
                if ilens.__class__.__name__=='Sersic':
                    for key in ['xc','yc','I_eff','r_eff','n','e1','e2']:
                        if not vars(ilens)[key].fixed:
                            self.ndim += 1
                            self.params_init.append(vars(ilens)[key].value)
                            self.params_name.append(key+'_lens_light'+str(i))
                            self.prior_type.append(vars(ilens)[key].prior_type)
                            self.prior_0.append(vars(ilens)[key].prior_0)
                            self.prior_1.append(vars(ilens)[key].prior_1)

        #check source
        for i,isource in enumerate(self.source_light):
            if isource.__class__.__name__=='Sersic':
                for key in ['xc','yc','I_eff','r_eff','n','e1','e2']:
                    if not vars(isource)[key].fixed:
                        self.ndim += 1
                        self.params_init.append(vars(isource)[key].value)
                        self.params_name.append(key+'_source_light'+str(i))
                        self.prior_type.append(vars(isource)[key].prior_type)
                        self.prior_0.append(vars(isource)[key].prior_0)
                        self.prior_1.append(vars(isource)[key].prior_1)
        #check pixelized source


    def apply_params_to_model(self,params):
        thislens_mass = copy.deepcopy(self.lens_mass)
        thislens_light = copy.deepcopy(self.lens_light)
        thissource = copy.deepcopy(self.source_light)
        
        #print('----0',params)
        #print('----0',self.prior_0)
        #print('----0',self.prior_1)
        params_index = 0
        for i,ilens in enumerate(thislens_mass):
            if ilens.__class__.__name__=='SIELens':
                for key in ['xc','yc','b_sie','e1','e2']:
                    if not vars(ilens)[key].fixed:
                        thislens_mass[i]._altered = True
                        thislens_mass[i].__dict__[key].value = params[params_index]
                        if not thislens_mass[i].__dict__[key].check_bound(): return False
                        params_index += 1

            elif ilens.__class__.__name__=='ExternalShear':
                for key in ['e1','e2']:
                    if not vars(ilens)[key].fixed:
                        thislens_mass[i]._altered = True
                        thislens_mass[i].__dict__[key].value = params[params_index]
                        if not thislens_mass[i].__dict__[key].check_bound(): return False
                        params_index += 1

        #check lens light
        if thislens_light is not None:
            for i,ilens in enumerate(thislens_light):
                if ilens.__class__.__name__=='Sersic':
                    for key in ['xc','yc','I_eff','r_eff','n','e1','e2']:
                        if not vars(ilens)[key].fixed:
                            thislens_light[i].__dict__[key].value = params[params_index]
                            if not thislens_light[i].__dict__[key].check_bound(): return False
                            params_index += 1

        #check source
        for i,isource in enumerate(thissource):
            if isource.__class__.__name__=='Sersic':
                for key in ['xc','yc','I_eff','r_eff','n','e1','e2']:
                    if not vars(isource)[key].fixed:
                        thissource[i].__dict__[key].value = params[params_index]
                        if not thissource[i].__dict__[key].check_bound(): return False
                        params_index += 1

        self.pass_math_relation(thislens_mass,thislens_light,thissource) #build the params math relation via this function
        
        return thislens_mass,thislens_light,thissource