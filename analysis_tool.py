import numpy as np 
import copy 
from RayTracing import Tracer
from matplotlib import pyplot as plt

def get_critical_line_and_caustic(lens,highresbox=[-2.,2.,-2.,2.],numres=0.01):
    """
    Routine to calculate the locations for the lensing caustics/critical line.      
    """
    
    # Figure out if we can do this analytically
    lens = list(np.array([lens]).flatten())
    #for l in lens: l._altered = True
    lens = [copy.deepcopy(l) for l in lens]
      
    # we calculate them numerically
    # first try to figure out where the caustics are going to be
    # based on position & einstein radii
    cximage = np.arange(highresbox[0],highresbox[1],numres)
    cyimage = np.arange(highresbox[2],highresbox[3],numres)
    cximage, cyimage = np.meshgrid(cximage,cyimage)
    tracer = Tracer(lens_mass=lens,lens_light=None,source=None,psf=None)
    xsource,ysource = tracer.ray_shoot(cximage, cyimage)
    jxy, jxx = np.gradient(xsource); jyy, jyx = np.gradient(ysource)
    A = jxx*jyy - jxy*jyx
    # it's pretty dumb that we have to do this...
    tmpfig = plt.figure(); dummyax = tmpfig.add_subplot(111)
    cset_caustic = dummyax.contour(xsource,ysource,A,levels=[0.])
    plt.close(tmpfig)
    contours = cset_caustic.collections[0].get_paths()
    caustics = []
    for contour in contours:
          xcon,ycon = contour.vertices[:,0], contour.vertices[:,1]
          caustic = np.vstack([xcon,ycon]).T
          caustics.append(caustic)

    tmpfig = plt.figure(); dummyax = tmpfig.add_subplot(111)
    cset_critical = dummyax.contour(cximage, cyimage,A,levels=[0.])
    plt.close(tmpfig)
    contours = cset_critical.collections[0].get_paths()
    criticals = []
    for contour in contours:
          xcon,ycon = contour.vertices[:,0], contour.vertices[:,1]
          critical = np.vstack([xcon,ycon]).T
          criticals.append(critical)
    for l in lens: l._altered = True
    return criticals,caustics