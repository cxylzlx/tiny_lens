import numpy as np 
import copy 
import util
from scipy import signal
from matplotlib import pyplot as plt

class Tracer(object):
    def __init__(self,lens_mass=None,lens_light=None,source=None,psf=None):
        """
        lens_mass,lens_light,source: each of then is a list of lensing model object
        """
        self.lens_mass = lens_mass
        self.lens_light = lens_light
        self.source = source
        self.psf = psf

    #@profile
    def total_deflection(self,x,y):
        alpha_x = np.copy(x)
        alpha_x *= 0
        alpha_y = np.copy(alpha_x)
        #print(np.any(np.isnan(alpha_x )))
        for ilens in self.lens_mass:
            ilens.deflect(x,y)
            #print('yyyyyyy',type(alpha_y))
            alpha_x += ilens.deflected_x
            alpha_y += ilens.deflected_y
            #print(np.any(np.isnan(ilens.deflected_x )))
        
        return (alpha_x,alpha_y)

    #@profile
    def ray_shoot(self,x,y):
        alpha_x,alpha_y = self.total_deflection(x,y)
        #print('hahaha',alpha_x,alpha_y)
        #print(np.any(np.isnan(alpha_x )))
        return x-alpha_x,y-alpha_y

    #@profile
    def generate_model_image(self,x,y,sub_grid=None):
        """
        x and y: the coordinates of grids, can be a 2-d array or 1-d array
        """
        mock_image = np.copy(x)*0.0 #np.zeros(x.shape)
        #lens light
        if self.lens_light is not None:
            for ilens in self.lens_light:
                mock_image += ilens.get_instensity(x,y)
        #ray-shoot the image grid
        xsrc,ysrc = self.ray_shoot(x,y)
        #print('--lens (x,y):',x[50,50],y[50,50])
        #print('--src (x,y):',xsrc[50,50],ysrc[50,50])
        #print(np.any(np.isnan(xsrc )))
        #print(np.any(np.isnan(ysrc )))
        #form mock image
        for isrc in self.source:
            mock_image += isrc.get_instensity(xsrc,ysrc)
        #check sub-grid
        if sub_grid is not None:
            mock_image = util.bin_image(mock_image,sub_grid=sub_grid)
        #check convolution
        if self.psf is not None:
            mock_image = signal.fftconvolve(mock_image, self.psf, mode='same')
        #iddd =  np.unravel_index(np.argmax(mock_image),mock_image.shape)
        #print('--source max index:',iddd)
        #print('--source max:',mock_image[iddd])
        #print('--source max x:',x[iddd])
        #print('--source max y:',y[iddd])
        #print('--src (x,y) max:',xsrc[iddd],ysrc[iddd])
        return mock_image    
   
class Tracer_1d(Tracer):
    def generate_model_image(self,x_1d,y_1d,shape2d,mask_id,sub_grid=None, mask_id_binned=None):
        """
        Here, x_1d and y_1d are 1-d position array.
        mask_id are the index of mask region calculated by np.where().
        """
        mock_image_1d = np.zeros(x_1d.shape)
        #lens light
        if self.lens_light is not None:
            for ilens in self.lens_light:
                mock_image_1d += ilens.get_instensity(x_1d,y_1d)
        #ray-shoot the image grid
        xsrc_1d,ysrc_1d = self.ray_shoot(x_1d,y_1d)
        #form mock image
        for isrc in self.source:
            mock_image_1d += isrc.get_instensity(xsrc_1d,ysrc_1d)
        #check convolution

        if sub_grid is None:  
            if self.psf is not None:
                #recover the mock image 2d
                mock_image_2d = np.zeros(shape2d)
                mock_image_2d[mask_id] = mock_image_1d
                #convolution
                mock_image_2d = signal.fftconvolve(mock_image_2d, self.psf, mode='same')
                return mock_image_2d[mask_id]
            else:
                return mock_image_1d
        else:
            #print('5555')
            mock_image_2d = np.zeros(shape2d)
            mock_image_2d[mask_id] = mock_image_1d
            mock_image_2d = util.bin_image(mock_image_2d,sub_grid)
            #print(mock_image_2d.shape,'-----')
            #print(self.psf)
            if self.psf is not None:
                mock_image_2d = signal.fftconvolve(mock_image_2d, self.psf, mode='same')
                return mock_image_2d[mask_id_binned]
            else:
                #print((mask_id_binned[0].size)**(0.5))
                return mock_image_2d[mask_id_binned]


