import numpy as np
import astropy.constants as co
import warnings

arcsec2rad = (np.pi/(180.*3600.))
rad2arcsec =3600.*180./np.pi
deg2rad = np.pi/180.

class Visdata(object):
    """
    Class to hold all necessary info relating to one set of visibilities.
    Auto-updates amp&phase or real&imag if those values are changed, but
    MUST SET WITH, eg, visobj.amp = (a numpy array of the new values);
    CANNOT USE, eg, visobj.amp[0] = newval, AS THIS DOES NOT CALL THE
    SETTER FUNCTIONS.
    
    Parameters:
    u     numpy ndarray
        The Fourier plane u coordinates of the visibilities to follow.
    v     numpy ndarray
        The Fourier plane v coordinates of the visibilities to follow.
    real  numpy ndarray
        The real parts of the visibilities
    imag  numpy ndarray
        The imaginary parts of the visibilities
    ant1  numpy ndarray
        The first antenna number or name of the visibility on each baseline
    ant2  numpy ndarray
        The second antenna number or name of the visibility on each baseline
    PBfwhm      float
        The FWHM of the antenna primary beam at this wavelength (at present
        assumes a homogeneous antenna array)
    filename    str
        A filename associated with these data.      
    """
    
    def __init__(self,u,v,real,imag,sigma,ant1=None,ant2=None,PBfwhm=None,filename=None):
        self.u = u
        self.v = v
        self.real = real
        self.imag = imag
        self.sigma = sigma
        self.ant1 = ant1
        self.ant2 = ant2
        self.PBfwhm = PBfwhm
        self.filename = filename
    
    @property
    def uvdist(self):
        return np.sqrt(self.u**2. + self.v**2.)

    @property
    def real(self):
        return self._real
    @real.setter
    def real(self,val):
        self._real = val
        # Setting amp & phase during __init__ will fail since imag is still unknown
        # Doing so during conjugate() will also fail, but gives a ValueError
        try:
            self._amp = np.sqrt(self._real**2. + self.imag**2.)
            self._phase = np.arctan2(self.imag,self._real)
        except (AttributeError,ValueError):
            self._amp = None
            self._phase = None
          
    @property
    def imag(self):
        return self._imag
    @imag.setter
    def imag(self,val):
        self._imag = val
        try:
            self._amp = np.sqrt(self.real**2. + self._imag**2.)
            self._phase = np.arctan2(self._imag,self.real)
        except (AttributeError,ValueError):
            self._amp = None
            self._phase = None

    @property 
    def amp(self):
        return  self._amp
    @amp.setter
    def amp(self,val):
        self._amp = val
        self._real = val * np.cos(self.phase)
        self._imag = val * np.sin(self.phase)

    @property
    def phase(self):
        return self._phase
    @phase.setter
    def phase(self,val):
        self._phase = val
        self._real = self.amp * np.cos(val)
        self._imag = self.amp * np.sin(val)

    def __add__(self,other):
        return Visdata(self.u,self.v,self.real+other.real,self.imag+other.imag,\
                (self.sigma**-2. + other.sigma**-2.)**-0.5)

    def __sub__(self,other):
        return Visdata(self.u,self.v,self.real-other.real,self.imag-other.imag,\
                (self.sigma**-2. + other.sigma**-2.)**-0.5)

    def conjugate(self):
        u = np.concatenate((self.u,-1.0*self.u))
        v = np.concatenate((self.v,-1.0*self.v))
        real = np.concatenate((self.real,self.real))
        imag = np.concatenate((self.imag,-1.0*self.imag))
        sigma = np.concatenate((self.sigma,self.sigma))
        ant1 = np.concatenate((self.ant1,self.ant2))
        ant2 = np.concatenate((self.ant2,self.ant1))
        self.u = u
        self.v = v
        self.real = real
        self.imag = imag
        self.sigma = sigma
        self.ant1 = ant1
        self.ant2 = ant2


def read_visdata(filename):
    """
    Function to read in visibility data from file and create a visdata object
    to hold it afterwards. So far only .bin files from get_visibilities.py are
    supported; idea is eventually to be able to not mess with that and get straight
    from a CASA ms, but don't currently know how to do that without bundling the 
    casacore utilities directly...

    Params:
    filename
        Name of file to read from. Should contain all the visibility data needed,
        including u (Lambda), v (Lambda), real, imag, sigma, antenna1, and antenna 2.

    Returns:
    visdata
        A visdata object containing the data from filename.
    """
    
    if not filename.split('.')[-1].lower() in ['bin']:
        raise ValueError('Only .bin files are supported for now...')

    data = np.fromfile(filename)
    PBfwhm = data[-1]
    data = data[:-1]
    data = data.reshape(7,int(data.size/7)) # bin files lose array shape, so reshape to match

    data = Visdata(*data,PBfwhm=PBfwhm,filename=filename)
    
    # Check for auto-correlations:
    if (data.u == 0).sum() > 0:
        warnings.warn("Found autocorrelations when reading the data (u == v == 0); removing them...")
        bad = data.u == 0
        data = Visdata(data.u[~bad],data.v[~bad],data.real[~bad],data.imag[~bad],data.sigma[~bad],
                data.ant1[~bad],data.ant2[~bad],data.PBfwhm,data.filename)
    
    # Check for flagged / otherwise bad data
    if (data.amp == 0).sum() > 0:
        warnings.warn("Found flagged/bad data when reading the data (amplitude == 0); removing them...")
        bad = data.amp == 0
        data = Visdata(data.u[~bad],data.v[~bad],data.real[~bad],data.imag[~bad],data.sigma[~bad],
                data.ant1[~bad],data.ant2[~bad],data.PBfwhm,data.filename)
    
    return data


def concatvis(visdatas):
    """
    Concatenate multiple visibility sets into one larger set.
    Does no consistency checking of any kind, so beware.
    
    :param visdatas:
        List of visdata objects

    This method returns:

    * ``concatvis'' - The concatenated visibility set.
    """

    newu, newv, newr, newi = np.array([]),np.array([]),np.array([]),np.array([])
    news, newa1,newa2 = np.array([]),np.array([]),np.array([])

    for vis in visdatas:
        newu = np.concatenate((newu,vis.u))
        newv = np.concatenate((newv,vis.v))
        newr = np.concatenate((newr,vis.real))
        newi = np.concatenate((newi,vis.imag))
        news = np.concatenate((news,vis.sigma))
        newa1= np.concatenate((newa1,vis.ant1))
        newa2= np.concatenate((newa2,vis.ant2))

    return Visdata(newu,newv,newr,newi,news,newa1,newa2,visdatas[0].PBfwhm,'Combined Data')     


def bin_visibilities(visdata,maxnewsize=None):
    """
    WARNING: DOESN'T WORK CURRENTLY(?)
    Bins up (ie, averages down) visibilities to reduce the total
    number of them.  Note that since we fit directly to the visibilities,
    this is slightly different (and easier) than gridding in preparation for
    imaging, as we won't need to FFT and so don't need a convolution function.

    :param visdata
          A Visdata object.
    :param maxnewsize = None
          If desired, the maximum number of visibilities post-binning can
          be specified. As long as this number meets other criteria (ie,
          we don't have bin sizes smaller than an integration time or
          bandwidth in wavelengths), the total number in the returned
          Visdata will have fewer than maxnewsize visibilities.

    This method returns:
    * ``BinnedVisibilities'' - A Visdata object containing binned visibilities.
    """

    if maxnewsize is None: maxnewsize = visdata.u.size/2

    # Bins should be larger than an integration; strictly only valid for an EW array,
    # and assumes a 20s integration time. Thus, this is a conservative estimate.
    minbinsize = 20. * visdata.uvdist.max() / (24*3600.)

    # Bins should be smaller than the effective field size
    maxbinsize = (visdata.PBfwhm * arcsec2rad)**-1

    print(minbinsize,maxbinsize)

    # We're going to find a binning solution iteratively; this gets us set up
    Nbins, binsizeunmet, Nvis, it, maxiter = [3000,3000], True, visdata.u.size, 0, 250
    
    while (binsizeunmet or Nvis >= maxnewsize):
        print(Nbins)
        # Figure out how to bin up the data
        counts,uedges,vedges,bins = stats.binned_statistic_2d(
                visdata.u,visdata.v,values=visdata.real,statistic='count',
                bins=Nbins)
        
        du, dv = uedges[1]-uedges[0], vedges[1]-vedges[0]

        # Check that our bins in u and v meet our conditions
        if (du > minbinsize and du < maxbinsize and
            dv > minbinsize and dv < maxbinsize): binsizeunmet = False
        # Otherwise we have to adjust the number of bins to adjust their size...
        #elif (du <= minbinsize or dv <= minbinsize): Nbins = int(Nbins/1.2)
        #elif (du >= maxbinsize or dv >= maxbinsize): Nbins = int(Nbins*1.2)
        elif du <= minbinsize: Nbins[0] = int(Nbins[0]/1.1); binsizeunmet=True
        elif dv <= minbinsize: Nbins[1] = int(Nbins[1]/1.1); binsizeunmet=True
        elif du >= maxbinsize: Nbins[0] = int(Nbins[0]*1.1); binsizeunmet=True
        elif dv >= maxbinsize: Nbins[1] = int(Nbins[1]*1.1); binsizeunmet=True

        # If we still have more than the desired number of visibilities, make
        # fewer bins (we'll loop after this).
        if np.unique(bins).size > maxnewsize: Nbins[0],Nbins[1] = int(Nbins[0]/1.1),int(Nbins[1]/1.1)
        Nvis = np.unique(bins).size
        it += 1
        if it > maxiter: raise ValueError("It's impossible to split your data into that few bins!  "
                                "Try setting maxnewsize to a larger value!")
        print(Nvis,du,dv)


    # Get us some placeholder arrays for the binned data
    u,v,real,imag,sigma,ant1,ant2 = np.zeros((7,Nvis))

    for i,filledbin in enumerate(np.unique(bins)):
        # This tells us which visibilities belong to the current bin
        points = np.where(bins==filledbin)[0]
        # This unravels the indices to uedges,vedges from the binned_statistic binnumber
        uloc = int(np.floor(filledbin/(vedges.size+1)) - 1)
        vloc = int(filledbin - (vedges.size+1)*(uloc+1) - 1)
        # Get our new data, place at center of uv bins
        u[i],v[i] = uedges[uloc]+0.5*du, vedges[vloc]+0.5*dv
        real[i],sumwt = np.average(visdata.real[points],weights=visdata.sigma[points]**-2.,returned=True)
        imag[i] = np.average(visdata.imag[points],weights=visdata.sigma[points]**-2.)
        sigma[i] = sumwt**-0.5
        # We can keep the antenna numbers if we've only selected points from the same baseline,
        # otherwise get rid of them (CHECK IF MODELCAL FAILS WITH None ANTENNAS)
        ant1[i] = visdata.ant1[points][0] if (visdata.ant1[points]==visdata.ant1[points][0]).all() else None
        ant2[i] = visdata.ant2[points][0] if (visdata.ant2[points]==visdata.ant2[points][0]).all() else None

    return Visdata(u,v,real,imag,sigma,ant1,ant2,visdata.PBfwhm,'BIN{0}'.format(Nvis)+visdata.filename) 