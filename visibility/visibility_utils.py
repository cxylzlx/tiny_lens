import numpy as np 
import copy
from scipy.fftpack import fftshift,fft2
from scipy.interpolate import RectBivariateSpline
import visibility.visibility_data as vl 
import scipy.sparse
from RayTracing import Tracer
from visibility.visibility_plot import uvimageslow
from PIL import Image

arcsec2rad = np.pi/(180.*3600.)
rad2arcsec =3600.*180./np.pi

def GenerateVisilensGrid(data=None,hw=None,dpix=None,sub_grid=None):
    """
    Routine to generate the grids for lensing. 

    Since we're going to be FFT'ing with these coordinates, the resolution isn't
    directly set-able. 

    Inputs:
    data:
        A Visdata object, used to determine the resolutions of 
        the two grids (based on the image size or maximum uvdistance in the dataset)
    hw:
        half size of the image (num of pixel)
    dpix: pixel size in arcsec
    """
    
    # Factors higher-resolution than (1/2*max(uvdist)) to make the field and emission grids
    #having ~4 resolution elements across the synthesized beam.
    Nover_field = 4.

    uvmax = data.uvdist.max()  #only for one dataset

    # Calculate resolutions of the grids
    if dpix is None: dpix = (2*Nover_field*uvmax)**-1.
    else: dpix *= arcsec2rad

    # Calculate the field grid size as a power of 2.
    Nfield = int(2**np.ceil(np.log2(2*hw)))  #2**np.ceil(np.log2(2*np.abs(hw)*arcsec2rad/dpix))
    #print(Nfield)
    # Calculate the grid coordinates for the larger field
    #print('----',hw)

    fieldcoords = np.linspace(-1.0*hw*dpix*rad2arcsec, hw*dpix*rad2arcsec, Nfield,endpoint=False)
    #print(fieldcoords/arcsec2rad,'ssss')
    xmapfield,ymapfield = np.meshgrid(fieldcoords,fieldcoords) 
    dpix_new = fieldcoords[1]-fieldcoords[0]  #new dpix !!!
    xmapfield += 0.5*dpix_new
    ymapfield += 0.5*dpix_new
    #print('-----',dpix_new)

    #shift pixel a little
    xshift = (xmapfield[0,1]-xmapfield[0,0])
    yshift = abs((ymapfield[1,0]-ymapfield[0,0]))

    if sub_grid is None:
        return xmapfield-xshift,ymapfield-yshift,None,None 
    else:
        #Note: need refactor; definition of hw is different for raido and optical lens modeling code
        hw1 = Nfield/2*sub_grid
        dpix1 = 1.0*dpix_new/sub_grid
        coord1 = np.arange(-hw1,hw1,1)*dpix1 + dpix1*0.5
        xsub,ysub = np.meshgrid(coord1,coord1)
        return xmapfield-xshift,ymapfield-yshift,xsub-xshift,ysub-yshift                    

def bin_image(arr, sub_grid=None):
    new_shape = (arr.shape[0]//sub_grid,arr.shape[1]//sub_grid)
    shape = (new_shape[0], sub_grid,
             new_shape[1], sub_grid)
    return arr.reshape(shape).mean(axis=(-1,1))
    

def generate_uv_grid_from_image(num_of_pix, dpix):
    kmax = 0.5/(dpix*arcsec2rad)
    return np.linspace(-kmax,kmax,num_of_pix)  #return u-v coordinates

def fft_interpolate(visdata,immap,xmap,ymap,ug=None,shiftphase=[0.,0.]):
    """
    Take a dataset and a map of a field, fft the image,
    and interpolate onto the uv-coordinates of the dataset.

    Returns:
    interpdata: Visdata object
          A dataset which samples the given immap
    """
    
    # Correct for PB attenuation            
    if visdata.PBfwhm is not None: 
        PBs = visdata.PBfwhm / (2.*np.sqrt(2.*np.log(2)))
        immap *= np.exp(-(xmap**2./(2.*PBs**2.)) - (ymap**2./(2.*PBs**2.)))
    
    #immap = immap[::-1,:] # Fixes issue of origin in tlc vs blc to match sky coords  
    imfft = fftshift(fft2(fftshift(immap)))
    
    # Calculate the uv points we need, if we don't already have them
    if ug is None:
        ug = generate_uv_grid_from_image(xmap.shape[0], (xmap[0,1]-xmap[0,0]))

    # Interpolate the FFT'd image onto the data's uv points
    # Using RBS, much faster since ug is gridded
    spliner = RectBivariateSpline(ug,ug,imfft.real,kx=1,ky=1)
    splinei = RectBivariateSpline(ug,ug,imfft.imag,kx=1,ky=1)
    interpr = spliner.ev(visdata.v,visdata.u) #The ax[0] is y/v coordinates
    interpi = splinei.ev(visdata.v,visdata.u)
    interpdata = vl.Visdata(visdata.u,visdata.v,interpr,interpi,visdata.sigma,\
                        visdata.ant1,visdata.ant2,visdata.PBfwhm,'interpolated_data')

    # Apply scaling, phase shifts; wrap phases to +/- pi.
    #interpdata.amp *= scaleamp
    interpdata.phase += 2.*np.pi*arcsec2rad*(shiftphase[0]*interpdata.u + shiftphase[1]*interpdata.v)
    interpdata.phase = (interpdata.phase + np.pi) % (2*np.pi) - np.pi

    return interpdata

def model_cal(realdata,modeldata,dPhi_dphi=None,FdPC=None):
    """
    Routine following Hezaveh+13 to implement perturbative phase corrections
    to model visibility data. This routine is designed to start out from an
    intermediate step in the self-cal process, since we can avoid doing a
    lot of expensive matrix inversions that way, but if either of the
    necessary arrays aren't provided, we calculate them.

    Inputs:
    realdata,modeldata:
        visdata objects containing the actual data and model-generated data

    dPhi_dphi: None, or pre-calculated
        See H+13, App A. An N_ant-1 x M_vis matrix whose ik'th element is 1
        if the first antenna of the visibility is k, -1 if the second, or 0.

    FdPC: None, or pre-calculated
        See H+13, App A, eq A2. This is an N_ant-1 x M_vis matrix, equal to
        -inv(F)*dPhi_dphi*inv(C) in the nomenclature of H+13. This has the 
        matrix inversions that we want to avoid calculating at every MCMC
        iteration (inverting C takes ~3s for M~5k, even with a sparse matrix).

    Outputs:
    modelcaldata:
        visdata object containing updated visibilities

    dphi:
        Array of length N_ant-1 containing the implemented phase offsets
    """

    # If we don't have the pre-calculated arrays, do so now. It's expensive
    # to do these matrix inversions at every MCMC step.
    if np.any((FdPC is None, dPhi_dphi is None)):
        uniqant = np.unique(np.asarray([realdata.ant1,realdata.ant2]).flatten())
        dPhi_dphi = np.zeros((uniqant.size-1,realdata.u.size))
        for j in range(1,uniqant.size):
            dPhi_dphi[j-1,:] = (realdata.ant1==uniqant[j])-1*(realdata.ant2==uniqant[j])
        C = scipy.sparse.diags((realdata.sigma/realdata.amp)**-2.,0)
        F = np.dot(dPhi_dphi,C*dPhi_dphi.T)
        Finv = np.linalg.inv(F)
        FdPC = np.dot(-Finv,dPhi_dphi*C)

    # Calculate current phase difference between data and model; wrap to +/- pi
    deltaphi = realdata.phase - modeldata.phase
    deltaphi = (deltaphi + np.pi) % (2 * np.pi) - np.pi

    dphi = np.dot(FdPC,deltaphi)

    modelcaldata = copy.deepcopy(realdata)
    
    modelcaldata.phase += np.dot(dPhi_dphi.T,dphi)

    return modelcaldata,dphi

def input_settings4modelling(
    data_file=None,
    hw=5.0,
    dpix=0.05,
    sub_grid=None,
    mask=None,
    modelcal=False,
    out_dir='./',
    dset_name='test'):
    data = vl.read_visdata(data_file)
    return {'data':data, 'hw':hw, 'dpix':dpix, 'sub_grid':sub_grid,'mask':mask, 
            'modelcal':modelcal, 'out_dir':out_dir, 'dset_name':dset_name}


#def GenerateVisilensGrid1(data=None,hw=None,dpix=None,sub_grid=None):
#    """
#    Routine to generate the grids for lensing. 
#
#    Since we're going to be FFT'ing with these coordinates, the resolution isn't
#    directly set-able. 
#
#    Inputs:
#    data:
#        A Visdata object, used to determine the resolutions of 
#        the two grids (based on the image size or maximum uvdistance in the dataset)
#    hw:
#        half size of the image in arcsec
#    dpix: pixel size in arcsec
#    """
#    
#    # Factors higher-resolution than (1/2*max(uvdist)) to make the field and emission grids
#    #having ~4 resolution elements across the synthesized beam.
#    Nover_field = 4.
#
#    uvmax = data.uvdist.max()  #only for one dataset
#
#    # Calculate resolutions of the grids
#    if dpix is None: dpix = (2*Nover_field*uvmax)**-1.
#    else: dpix *= arcsec2rad
#
#    # Calculate the field grid size as a power of 2.
#    Nfield = 2**np.ceil(np.log2(2*np.abs(hw)*arcsec2rad/dpix))
#    #print(Nfield)
#    # Calculate the grid coordinates for the larger field
#
#    fieldcoords = np.linspace(-np.abs(hw),np.abs(hw),Nfield)
#    #print(hw)
#    #print(fieldcoords,'ssss')
#    xmapfield,ymapfield = np.meshgrid(fieldcoords,fieldcoords) 
#    #print(xmapfield,'ssss')
#    dpix_new = fieldcoords[1]-fieldcoords[0]  #new dpix !!!
#    xmapfield += 0.5*dpix_new
#    ymapfield += 0.5*dpix_new
#    #print('-----',dpix_new)
#
#    #shift pixel a little
#    xshift = (xmapfield[0,1]-xmapfield[0,0])
#    yshift = abs((ymapfield[1,0]-ymapfield[0,0]))
#
#    if sub_grid is None:
#        return xmapfield-xshift,ymapfield-yshift,None,None 
#    else:
#        #Note: need refactor; definition of hw is different for raido and optical lens modeling code
#        hw1 = Nfield/2*sub_grid
#        dpix1 = 1.0*dpix_new/sub_grid
#        coord1 = np.arange(-hw1,hw1,1)*dpix1 + dpix1*0.5
#        xsub,ysub = np.meshgrid(coord1,coord1)
#        return xmapfield-xshift,ymapfield-yshift,xsub-xshift,ysub-yshift 

def get_optimium_dpix_and_hw(dset):
    # Factors higher-resolution than (1/2*max(uvdist)) 
    # to make the field and emission grids
    Nover_field = 4.
    uvmax = dset.uvdist.max()
    dpix = (2*Nover_field*uvmax)**-1.
    dpix *= rad2arcsec
    
    kmax = 0.5/(dpix*arcsec2rad)
    hw = kmax/(dset.uvdist.min())
    hw =  2**np.ceil(np.log2(hw))
    return dpix,hw

def return_dataset_sn_level(dset,fac_largest=0.1):
    amp_sn = dset.amp/dset.sigma
    N = int(amp_sn.size * fac_largest)
    amp_sn_sub = amp_sn[np.argsort(amp_sn)[-N:]]
    #real_sn = dset.real/dset.sigma
    #imag_sn = dset.imag/dset.sigma
    #return max(real_sn.max(),imag_sn.max())
    #print('check visidata',np.array_equal(dset.amp,np.sqrt(dset.real**2+dset.imag**2)))
    return amp_sn_sub.mean()

def write_dset_to_bin_file(dset,outfname='mock_visi_data.bin'):
    allarr = np.vstack((dset.u, dset.v, dset.real,
            dset.imag, dset.sigma, dset.ant1, dset.ant2))

    with open(outfname,'wb')as f:
        allarr.tofile(f)
        f.write(dset.PBfwhm)
