import copy
import util
from visibility.visibility_utils import GenerateVisilensGrid,generate_uv_grid_from_image
import os

class Abstractdataloading(object):
    """
    An Abstract data loading class
    Include below components:
    1. accept observational data
    2. accept the lens/source model used for modelling
    """

    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0):
        """
        data_dict: the observation data dict 
        lens_mass: lens mass model list 
        lens_light: lens light model list
        source: source model list
        positions: The position pairs are required to tracing back to a small region in source-plane
        hw: the half-width of image. used for cutting image to improve the modelling speed
        """
        self.dset = data_dict['data']
        self.hw = data_dict['hw']
        self.dpix = data_dict['dpix'] 
        self.modelcal = data_dict['modelcal']
        self.sub_grid = data_dict['sub_grid']
        self.out_dir = data_dict['out_dir']
        self.dset_name = data_dict['dset_name']
        self.multinest_outdir = '{}/{}/output_multinest/'.format(self.out_dir,self.dset_name)
        if not os.path.exists(self.multinest_outdir):
            os.makedirs(self.multinest_outdir)
            
        #self.mask = data_dict['mask']  mask is set in modelling file
        self.psf = None
        
        self.lens_mass = copy.deepcopy(lens_mass)
        self.lens_light = copy.deepcopy(lens_light)
        self.source_light = copy.deepcopy(source_light)
        self.ndim = 0
        self.params_init = []
        self.params_name = []

        self.x, self.y, self.xsub, self.ysub =  GenerateVisilensGrid(
            data=self.dset,
            hw=self.hw,
            dpix=self.dpix,
            sub_grid=self.sub_grid
        )
        self.dpix = self.x[0,1]-self.x[0,0] #the formally dpix value we set may change. bescause we want to use fft
        self.ug = generate_uv_grid_from_image(self.x.shape[0],self.dpix)

        self.positions = positions
        self.threshhold = threshhold
        self.prior_type = []
        self.prior_0 = []
        self.prior_1 = []

