from RayTracing import Tracer
from matplotlib import pyplot as plt
import numpy as np 
from PIL import Image
from visibility.visibility_plot import uvimageslow
import copy
from visibility.visibility_utils import fft_interpolate,model_cal
from scipy.ndimage.measurements import center_of_mass
from matplotlib.colors import SymLogNorm
import util
from RayTracing import Tracer
from mpl_toolkits.axes_grid1 import make_axes_locatable
from analysis_tool import get_critical_line_and_caustic

def visualizer(fitter,params,positions=None,outfile=None,mark_pos=False,src_image_option=[500, 0.01, 2.0, 2.0],**kwargs):
    """
    fitter: fitter object
    params: fitting params results
    """
    import matplotlib.cm as cm
    from matplotlib import pyplot as plt
    plt.style.use('seaborn-paper')
    plt.rc('xtick', labelsize='medium')
    plt.rc('ytick', labelsize='medium')
    plt.rc('text', usetex=True)
    font = {'family':'serif', 'size':25}
    plt.rc('font', **font)
    plt.rc('axes', labelsize='medium', lw=1, facecolor='None', edgecolor='k')

    thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(params)
    tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
    if fitter.sub_grid is None:
        mock_image = tracer.generate_model_image(fitter.x,fitter.y)
    else:
        mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)

    immap = np.zeros(fitter.x.shape)
    immap[:,:] += mock_image
    
    imsize = mock_image.shape[0]
    pixsize = fitter.x[0,1] - fitter.x[0,0]
    ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]
    source_xgrid,source_ygrid = tracer.ray_shoot(fitter.x,fitter.y)

    sgridx_reg,sgridy_reg,_,_ = util.generate_grid(hw=src_image_option[0],dpix=src_image_option[1])
    ext_src = [sgridx_reg.min(), sgridx_reg.max(), sgridy_reg.min(), sgridy_reg.max()]
    I_source = np.zeros(sgridx_reg.shape)
    for item in thissource:
        I_source += item.get_instensity(sgridx_reg,sgridy_reg)

    fig,ax = plt.subplots(nrows=1,ncols=2)
    im0 = ax[0].imshow(immap,extent=ext,cmap='jet',origin='lower')
    divider0 = make_axes_locatable(ax[0])
    cax0 = divider0.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im0, cax=cax0)

    im1 = ax[1].imshow(I_source,extent=ext_src,cmap='jet',origin='lower')
    divider1 = make_axes_locatable(ax[1])
    cax1 = divider1.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im1, cax=cax1)   
    ax[1].scatter(source_xgrid,source_ygrid,s=0.1,color='k')

    if positions is not None:
        for pos_pair in positions:
            posx = np.array([item[0] for item in pos_pair])
            posy = np.array([item[1] for item in pos_pair])
            src_pos_pair = [tracer.ray_shoot(item[0],item[1]) for item in pos_pair]
            srcx_pos = np.array([item[0] for item in src_pos_pair])
            srcy_pos = np.array([item[1] for item in src_pos_pair])

            ax[0].plot(posx,posy,'*')
            ax[1].plot(srcx_pos,srcy_pos,'*')

    if outfile is None:
        plt.show()
    else:
        plt.savefig(outfile+'_ideal.png',dpi=200,bbox_inches='tight')
    plt.close(fig=fig)

    if mark_pos:
        util.get_position_from_image(immap, extent=ext)

    immap = immap[::-1,:]  #flip the image, to match the inverse transformation of the visibility data
    best_fit_ideal_image = np.copy(immap)
    immap *= (fitter.x[0,1]-fitter.x[0,0])**2.
    interpdata = fft_interpolate(fitter.dset, immap, fitter.x, fitter.y, fitter.ug)

    if fitter.modelcal: 
        selfcal,_ = model_cal(fitter.dset,interpdata)
    else: 
        selfcal = copy.deepcopy(fitter.dset)
    
    #data_dirty_image = np.random.rand(imsize,imsize) #uvimageslow(selfcal,imsize, pixsize)
    #model_dirty_image = np.random.rand(imsize,imsize) #uvimageslow(interpdata,imsize, pixsize)
    data_dirty_image = uvimageslow(selfcal,imsize, pixsize)
    model_dirty_image = uvimageslow(interpdata,imsize, pixsize)
    imdiff = data_dirty_image - model_dirty_image
    criticals,caustics = get_critical_line_and_caustic(thislens_mass,highresbox=[-3.,3.,-3.,3.],numres=0.001)

    #visualization
    limits = kwargs.pop('limits',ext)
    cmap = kwargs.pop('cmap', 'jet') #cm.Greys)
    mapcontours = kwargs.pop('mapcontours',np.delete(np.arange(-21,22,3),7))
    rescontours = kwargs.pop('rescontours',np.array([-6,-5,-4,-3,-2,2,3,4,5,6]))
    level = kwargs.pop('level',None)
    logmodel = kwargs.pop('logmodel',False)

    subplot_size = 4
    fig = plt.figure(figsize=(9*subplot_size,5*subplot_size),)
    f_ax0 = fig.add_subplot(231)
    f_ax1 = fig.add_subplot(232)
    f_ax2 = fig.add_subplot(233)
    f_ax3 = fig.add_subplot(234)
    f_ax4 = fig.add_subplot(235)
    
    if level is None: 
        level = ((fitter.dset.sigma**-2.).sum())**-0.5
    else:
        level = float(level)

    if np.log10(level) < -6.: 
        sig_level,unit = 1e9*level,r'nJy'
        scale = 1e9
    elif np.log10(level) < -3.: 
        sig_level,unit = 1e6*level,r'$\mu$Jy'
        scale = 1e6
    elif np.log10(level) < 0.: 
        sig_level,unit = 1e3*level,r'mJy'
        scale = 1e3
    else: 
        sig_level,unit = level,r'Jy'
        scale = 1.0

    if logmodel: norm=SymLogNorm(0.01*data_dirty_image.max()*scale) #imemit = np.log10(imemit); vmin = imemit.min()-2.
    else: norm=None #vmin = imemit.min()
    im0 = f_ax0.imshow(data_dirty_image*scale,interpolation='nearest',extent=ext,cmap=cmap,norm=norm)
    divider0 = make_axes_locatable(f_ax0)
    cax0 = divider0.append_axes('right', size="5%", pad=0.05) #'bottom'
    cb = fig.colorbar(im0, cax=cax0, orientation='vertical') #'horizontal'
    cb.ax.minorticks_on()
    f_ax0.contour(data_dirty_image*scale,extent=ext,colors='k',origin='image',levels=sig_level*mapcontours)
    f_ax0.minorticks_on() 
    f_ax0.set_xlim(limits[0],limits[1])
    f_ax0.set_ylim(limits[2],limits[3])
    f_ax0.set_xlabel('Arcsec') 
    f_ax0.set_ylabel('Arcsec') 
    f_ax0.set_title('Data',fontsize=25)
    #plot position in dirty data image
    if positions is not None:
        for pos_pair in positions:
            posx = np.array([item[0] for item in pos_pair])
            posy = np.array([item[1] for item in pos_pair])
            f_ax0.plot(posx,posy,'*')

    if logmodel: norm=SymLogNorm(0.01*model_dirty_image.max()*scale) #imemit = np.log10(imemit); vmin = imemit.min()-2.
    else: norm=None #vmin = imemit.min()
    im1 = f_ax1.imshow(model_dirty_image*scale,interpolation='nearest',extent=ext,cmap=cmap,norm=norm)
    divider1 = make_axes_locatable(f_ax1)
    cax1 = divider1.append_axes('right', size="5%", pad=0.05)
    cb = fig.colorbar(im1, cax=cax1, orientation='vertical')  
    cb.ax.minorticks_on()
    f_ax1.contour(model_dirty_image*scale,extent=ext,colors='k',origin='image',levels=sig_level*mapcontours)
    f_ax1.minorticks_on()
    f_ax1.set_xlim(limits[0],limits[1])
    f_ax1.set_ylim(limits[2],limits[3])
    f_ax1.set_xlabel('Arcsec') 
    f_ax1.set_ylabel('Arcsec') 
    f_ax1.set_title('Model',fontsize=25)
    #plot lens center and sersic source center
    lens_xc = []; lens_yc = []
    for l in thislens_mass:
        try:
            lens_xc.append(l.xc.value)
            lens_yc.append(l.yc.value)
        except:
            pass
    src_xc = [s.xc.value for s in thissource]
    src_yc = [s.yc.value for s in thissource]
    
    f_ax1.plot(lens_xc, lens_yc,'r^')
    f_ax1.plot(src_xc,src_yc,'go')


    if logmodel: norm=SymLogNorm(0.01*imdiff.max()*scale) #imemit = np.log10(imemit); vmin = imemit.min()-2.
    else: norm=None #vmin = imemit.min()
    im2 = f_ax2.imshow(imdiff*scale,interpolation='nearest',extent=ext,cmap=cmap, norm=norm)
    divider2 = make_axes_locatable(f_ax2)
    cax2 = divider2.append_axes('right', size="5%", pad=0.05)
    cb = fig.colorbar(im2, cax=cax2, orientation='vertical')
    cb.ax.minorticks_on()
    f_ax2.contour(imdiff*scale,extent=ext,colors='k',origin='image',levels=sig_level*rescontours)
    f_ax2.minorticks_on() 
    f_ax2.set_xlim(limits[0],limits[1])
    f_ax2.set_ylim(limits[2],limits[3])
    f_ax2.set_xlabel('Arcsec') 
    f_ax2.set_ylabel('Arcsec') 
    f_ax2.set_title('Image-residual',fontsize=25)
    

    if logmodel: norm=SymLogNorm(0.01*best_fit_ideal_image.max()) #imemit = np.log10(imemit); vmin = imemit.min()-2.
    else: norm=None #vmin = imemit.min()
    percent = [0.001,99.999]
    vmin = np.percentile(best_fit_ideal_image,percent[0]) 
    vmax = np.percentile(best_fit_ideal_image,percent[1]) 
    im3 = f_ax3.imshow(best_fit_ideal_image,interpolation='nearest',extent=ext,cmap=cmap,norm=norm,vmin=vmin,vmax=vmax)
    divider3 = make_axes_locatable(f_ax3)
    cax3 = divider3.append_axes('right', size="5%", pad=0.05)
    cb = fig.colorbar(im3, cax=cax3, orientation='vertical')
    cb.ax.minorticks_on()
    xcen = center_of_mass(immap)[1]*fitter.dpix + fitter.x.min()
    ycen = -center_of_mass(immap)[0]*fitter.dpix + fitter.y.max()
    dx = 0.5*(limits[1]-limits[0])
    dy = 0.5*(limits[3]-limits[2])
    f_ax3.minorticks_on() 
    f_ax3.set_xlim(xcen-dx,xcen+dx)
    f_ax3.set_ylim(ycen-dy,ycen+dy)
    f_ax3.plot(lens_xc, lens_yc,'r^')
    f_ax3.plot(src_xc,src_yc,'go')
    for critical in criticals:
        f_ax3.plot(critical[:,0],critical[:,1],'k--')
    f_ax3.set_xlabel('Arcsec') 
    f_ax3.set_ylabel('Arcsec') 
    f_ax3.set_title('Sky-Model',fontsize=25)

    if logmodel: norm=SymLogNorm(0.01*I_source.max()) #imemit = np.log10(imemit); vmin = imemit.min()-2.
    else: norm=None #vmin = imemit.min()
    percent = [0.001,99.999]
    vmin = np.percentile(I_source,percent[0]) 
    vmax = np.percentile(I_source,percent[1]) 
    im4 = f_ax4.imshow(I_source,interpolation='nearest',extent=ext_src,cmap=cmap,norm=norm, origin='lower',vmin=vmin,vmax=vmax)
    divider4 = make_axes_locatable(f_ax4)
    cax4 = divider4.append_axes('right', size="5%", pad=0.05)
    cb = fig.colorbar(im4, cax=cax4, orientation='vertical')
    cb.ax.minorticks_on()
    #f_ax4.plot(src_xc,src_yc,'go')
    xcen = center_of_mass(I_source)[1]*src_image_option[1] + ext_src[0]
    ycen = -center_of_mass(I_source)[0]*src_image_option[1] + ext_src[3]
    dx = 0.5*src_image_option[2]
    dy = 0.5*src_image_option[3]
    f_ax4.minorticks_on() 
    f_ax4.set_xlim(xcen-dx,xcen+dx)
    f_ax4.set_ylim(ycen-dy,ycen+dy)
    for caustic in caustics:
        f_ax4.plot(caustic[:,0],caustic[:,1],'k--')
    if positions is not None:
        f_ax4.plot(srcx_pos,srcy_pos,'g*')
    f_ax4.set_xlabel('Arcsec') 
    f_ax4.set_ylabel('Arcsec') 
    f_ax4.set_title('Source',fontsize=25)


    # Label some axes and such
    redisual_level = imdiff.std()
    if np.log10(redisual_level) < -6.: sig_redisual,unit_redisual = 1e9*redisual_level,r'nJy'
    elif np.log10(redisual_level) < -3.: sig_redisual,unit_redisual = 1e6*redisual_level,r'$\mu$Jy'
    elif np.log10(redisual_level) < 0.: sig_redisual,unit_redisual = 1e3*redisual_level,r'mJy'
    else: sig_redisual,unit_redisual = redisual_level,r'Jy'
    f_ax2.text(0.1,0.1,r"Noise-level = {0:.1f}{1:s}".format(sig_level,unit)+
                "\n"+
                "Res-level = {0:.1f}{1:s}".format(sig_redisual,unit_redisual),
            transform=f_ax2.transAxes,bbox=dict(fc='w'), fontdict={'family':'serif', 'size':20})
    print('-----------------------')
    if outfile is None:
        fig.savefig('result.pdf',format='pdf',bbox_inches='tight') #300dpi for png file
        #plt.show()
    else:
        fig.savefig(outfile+'_result.pdf',format='pdf',bbox_inches='tight')
        