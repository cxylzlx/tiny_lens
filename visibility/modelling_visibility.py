import numpy as np 
import scipy
import copy
import util
from RayTracing import Tracer
from RayTracing import Tracer_1d
from visibility.analyze_fit_params_visibility import AnalyzeFitParams
from visibility.visibility_utils import fft_interpolate,model_cal
from PIL import Image
import os

class AbstractFitRoutine(AnalyzeFitParams): 
    """
    An Abstract fitness routine class
    Include below components:
    1. accept observational data
    2. accept the lens/source model used for modelling
    3. extract the fitting parameter list from input lens/source model
    4. Apply the changing parameter list to input lens/source model, check bound
    5. ray-tracing,generate mock image, compare with observation(chi-square)
    Note: the more specifically method (likelihood,prior etc) are defined in another class which
    inherit this class 
    """
    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0):

        super(AbstractFitRoutine,self).__init__(
            data_dict=data_dict,
            lens_mass=lens_mass,
            lens_light=lens_light,
            source_light=source_light,
            positions=positions,
            threshhold=threshhold
        )        
        #initialize fitting params/setting
        self.get_params_from_model()
        self.dof = self.dset.real.size*2 - self.ndim
        print('dof is:',self.dof)
        print('initial parameter value',self.params_init)

        # Get any model-cal parameters set up. The process involves some expensive
        # matrix inversions, but these only need to be done once, so we'll do them
        # now and pass the results as arguments to the likelihood function. See docs
        # in calc_likelihood.model_cal for more info.
        #print('---modelcal---',self.modelcal)
        if self.modelcal:
            uniqant = np.unique(np.asarray([self.dset.ant1,self.dset.ant2]).flatten())  #find all unique antena id 
            #print(uniqant)
            dPhi_dphi = np.zeros((uniqant.size-1,self.dset.u.size))  #[num_of_unique_antena-1, number_of_uv_baseline]
            for j in range(1,uniqant.size):
                dPhi_dphi[j-1,:]=(self.dset.ant1==uniqant[j])-1*(self.dset.ant2==uniqant[j]) #only have 0,1,-1 elements
            C = scipy.sparse.diags((self.dset.sigma/self.dset.amp)**-2.,0)
            F = np.dot(dPhi_dphi,C*dPhi_dphi.T)
            Finv = np.linalg.inv(F)
            FdPC = np.dot(-Finv,dPhi_dphi*C)
            self.modelcal = [dPhi_dphi,FdPC]  #!!!!!change self.modelcal
            #print('dPhi_dphi---ini',dPhi_dphi)  #right
            #print('FdPC---ini',FdPC) #right

        #print('likelihood value is',self.chi2(self.params_init)[0]*2.0)
        

    def generate_visilens_image(self,tracer,params,sub_grid=None):
        if sub_grid is None:
            mock_image_highres = tracer.generate_model_image(self.x,self.y,sub_grid=self.sub_grid)
        else:
            mock_image_highres = tracer.generate_model_image(self.xsub,self.ysub,sub_grid=self.sub_grid)

        #imsrc = Image.fromarray(mock_image_highres)
        #resize = np.array(imsrc.resize((int(self.indices[1]-self.indices[0]),int(self.indices[3]-self.indices[2])),Image.ANTIALIAS)) 
        #print('1111111',mock_image_highres.shape)
        #print(self.sub_grid)
        #print(self.xsub.shape)
        immap = np.zeros(self.x.shape)
        immap[:,:] += mock_image_highres #resize
        immap = immap[::-1,:]  #flip the image, to match the inverse transformation of the visibility data
        immap *= (self.x[0,1]-self.x[0,0])**2.

        return immap


    #@profile   
    def chi2(self,params,return_mock=False):
        x = self.apply_params_to_model(params)
        try: thislens_mass,thislens_light,thissource = x
        except TypeError: return util.INF_CHI2,None
        tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=self.psf)
        #check posiitons
        if self.positions is not None:
            res = self.check_positions(tracer=tracer)
            if not res: return util.INF_CHI2,None
           
        immap = self.generate_visilens_image(tracer,params,self.sub_grid)


        #the diffrent is from: the resize unti-aliasing process.
        #also, there is an offset in coordinates comparing with previous code
        #idd = np.unravel_index(np.argmax(immap),immap.shape)
        #print('coords of max image value',self.x[idd],self.y[idd])
        #print('dpix',self.dpix)
        #print('pixel (0,0) coords value',self.x[0,1],self.y[1,0])
        #from matplotlib import pyplot as plt 
        #plt.figure()
        #plt.imshow(immap,cmap='jet')
        #plt.colorbar()
        #plt.show()

        interpdata = fft_interpolate(self.dset, immap, self.x, self.y, self.ug)

        lnL = 0.
        dphase = None
        # If desired, do model-cal on this dataset
        if self.modelcal:
            modeldata,dphase = model_cal(self.dset,interpdata,self.modelcal[0],self.modelcal[1])

            lnL += (((modeldata.real - interpdata.real)**2. + (modeldata.imag - interpdata.imag)**2.)/modeldata.sigma**2.).sum()             
        # Calculate the contribution to chi2 from this dataset
        else: lnL += (((self.dset.real - interpdata.real)**2. + (self.dset.imag - interpdata.imag)**2.)/self.dset.sigma**2.).sum()
        #print('dphase2-----',dphase) #small diffrence, should be right
        return lnL,dphase

    #@profile
    def check_positions(self,tracer=None):
        for pos in self.positions:
            point_0 = pos[0]
            point_1 = pos[1]

            point_0_s = tracer.ray_shoot(point_0[0],point_0[1])
            point_1_s = tracer.ray_shoot(point_1[0],point_1[1])

            dist = np.sqrt((point_0_s[0]-point_1_s[0])**2 + (point_0_s[1]-point_1_s[1])**2)
            #print('distance----',dist,'-----',pos)
            if dist > self.threshhold:
                #print('position works','distance----',dist,'-----',pos)
                return False
            #print('fullil position requirement')
        return True


class AbstractFitRoutine_1d(AbstractFitRoutine):

    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0,hw=None,
                default_mask_radii=3.):
   
        super(AbstractFitRoutine_1d,self).__init__(
            data_dict=data_dict,
            lens_mass=lens_mass,
            lens_light=lens_light,
            source_light=source_light,
            positions=positions,
            threshhold=threshhold 
        )
            
        if data_dict['mask'] is None:
            self.mask = util.generate_mask(self.x,self.y,default_mask_radii)
            if self.sub_grid is not None:
                self.mask_sub = util.generate_mask(self.xsub,self.ysub,default_mask_radii)
        else:
            self.mask = util.cut_image_around_center(data_dict['mask'],self.hw)
            if self.sub_grid is not None:
                self.mask_sub = util.generate_submask_from_mask(self.mask, self.xsub.shape, sub_grid=self.sub_grid)


        #util.matplot_show(self.mask.astype('float'))
        self.mask_id = np.where(self.mask==0)
        self.x_1d =  np.copy(self.x[self.mask_id])
        self.y_1d =  np.copy(self.y[self.mask_id])

        if self.sub_grid is not None:
            self.mask_id_sub = np.where(self.mask_sub==0)
            #print(self.mask_id_sub,'---------')
            self.x_1d_sub = np.copy(self.xsub[self.mask_id_sub])
            self.y_1d_sub = np.copy(self.ysub[self.mask_id_sub]) 


    def chi2(self,params,return_mock=False):
        x = self.apply_params_to_model(params)
        try: thislens_mass,thislens_light,thissource = x
        except TypeError: return util.INF_CHI2,None
        #print('111111111111111')
        #print(self.x_1d.size)
        tracer = Tracer_1d(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=self.psf)
        #print('22222222')
        #check posiitons
        if self.positions is not None:
            res = self.check_positions(tracer=tracer)
            #print('33333333333333')
            if not res: return util.INF_CHI2,None
        if self.sub_grid is None:
            mock_image_highres_1D = tracer.generate_model_image(self.x_1d,self.y_1d,self.x.shape,self.mask_id)     
        else:
            #print('4444444444')
            mock_image_highres_1D = tracer.generate_model_image(self.x_1d_sub,self.y_1d_sub, self.xsub.shape, self.mask_id_sub, 
                                                        sub_grid=self.sub_grid, mask_id_binned=self.mask_id)            
        mock_image_highres_2d = np.zeros(self.x.shape)
        mock_image_highres_2d[self.mask_id] +=  mock_image_highres_1D
        #imsrc = Image.fromarray(mock_image_highres_2d)
        #resize = np.array(imsrc.resize((int(self.indices[1]-self.indices[0]),int(self.indices[3]-self.indices[2])),Image.ANTIALIAS)) 
        immap = np.zeros(self.x.shape)
        immap[:,:] += mock_image_highres_2d #resize
        immap = immap[::-1,:]  #flip the image, to match the inverse transformation of the visibility data
        immap *= (self.x[0,1]-self.x[0,0])**2.

        interpdata = fft_interpolate(self.dset, immap, self.x, self.y, self.ug)

        lnL = 0.
        dphase = None
        # If desired, do model-cal on this dataset
        if self.modelcal:
            modeldata,dphase = model_cal(self.dset,interpdata,self.modelcal[0],self.modelcal[1])
            #dphases[i] = dphase
            lnL += (((modeldata.real - interpdata.real)**2. + (modeldata.imag - interpdata.imag)**2.)/modeldata.sigma**2.).sum()
              
        # Calculate the contribution to chi2 from this dataset
        else: lnL += (((self.dset.real - interpdata.real)**2. + (self.dset.imag - interpdata.imag)**2.)/self.dset.sigma**2.).sum()

        return lnL,dphase