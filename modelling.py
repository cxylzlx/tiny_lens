import numpy as np 
import copy
import util
from RayTracing import Tracer
from RayTracing import Tracer_1d
from analyze_fit_params import AnalyzeFitParams


class AbstractFitRoutine(AnalyzeFitParams): 
    """
    An Abstract fitness routine class
    Include below components:
    1. accept observational data
    2. accept the lens/source model used for modelling
    3. extract the fitting parameter list from input lens/source model
    4. Apply the changing parameter list to input lens/source model, check bound
    5. ray-tracing,generate mock image, compare with observation(chi-square)
    Note: the more specifically method (likelihood,prior etc) are defined in another class which
    inherit this class 
    """
    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0,hw=None):

        super(AbstractFitRoutine,self).__init__(
            data_dict=data_dict,
            lens_mass=lens_mass,
            lens_light=lens_light,
            source_light=source_light,
            positions=positions,
            threshhold=threshhold,
            hw=hw           
        )        
        #initialize fitting params
        self.get_params_from_model()
        self.dof = self.ccd_image.size - self.ndim #degree of freedom
       
        
        
    #@profile   
    def chi2(self,params,return_mock=False):
        #print('-----111-------------',self.ndim)
        #print('-----222------------',len(params))
        x = self.apply_params_to_model(params)
        try: thislens_mass,thislens_light,thissource = x
        except TypeError: return util.INF_CHI2  #bug fixed
        tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=self.psf)
        #check posiitons
        if self.positions is not None:
            res = self.check_positions(tracer=tracer)
            #print('hahahahaha')
            if not res: return util.INF_CHI2
        if self.sub_grid is None:    
            mock_image = tracer.generate_model_image(self.x,self.y,sub_grid=self.sub_grid)
        else:
            mock_image = tracer.generate_model_image(self.xsub,self.ysub,sub_grid=self.sub_grid)
        #print(mock_image.shape,'hhhh')
        #util.matplot_show(mock_image)
        if return_mock:
            return np.sum((self.ccd_image-mock_image)**2*self.weight),mock_image
        else:
            return np.sum((self.ccd_image-mock_image)**2*self.weight)

    #@profile
    def check_positions(self,tracer=None):
        for pos in self.positions:
            point_0 = pos[0]
            point_1 = pos[1]
            #print('point 0---',point_0)
            #print('point 1---',point_1)
            point_0_s = tracer.ray_shoot(point_0[0],point_0[1])
            point_1_s = tracer.ray_shoot(point_1[0],point_1[1])
            #print('point source 0---',point_0_s)
            #print('point source 1---',point_1_s)
            dist = np.sqrt((point_0_s[0]-point_1_s[0])**2 + (point_0_s[1]-point_1_s[1])**2)
            #print('distance----',dist)
            if dist > self.threshhold:
                #print('position works')
                return False
        return True


class AbstractFitRoutine_1d(AbstractFitRoutine):

    def __init__(self, data_dict=None,lens_mass=None,lens_light=None,source_light=None,positions=None,threshhold=1.0,hw=None,
                default_mask_radii=2.5):
   
        super(AbstractFitRoutine_1d,self).__init__(
            data_dict=data_dict,
            lens_mass=lens_mass,
            lens_light=lens_light,
            source_light=source_light,
            positions=positions,
            threshhold=threshhold,
            hw=hw           
        )
            
        if data_dict['mask'] is None:
            self.mask = util.generate_mask(self.x,self.y,default_mask_radii)
            if self.sub_grid is not None:
                self.mask_sub = util.generate_mask(self.xsub,self.ysub,default_mask_radii)
        else:
            self.mask = util.cut_image_around_center(data_dict['mask'],self.hw)
            if self.sub_grid is not None:
                self.mask_sub = util.generate_submask_from_mask(self.mask, self.xsub.shape, sub_grid=self.sub_grid)

        self.mask_id = np.where(self.mask==0)
        self.ccd_image_1d = self.ccd_image[self.mask_id]
        self.weight_1d = self.weight[self.mask_id]
        self.x_1d = np.copy(self.x[self.mask_id])
        self.y_1d = np.copy(self.y[self.mask_id])  

        if self.sub_grid is not None:
            self.mask_id_sub = np.where(self.mask_sub==0)
            self.x_1d_sub = np.copy(self.xsub[self.mask_id_sub])
            self.y_1d_sub = np.copy(self.ysub[self.mask_id_sub])          

        self.dof = self.ccd_image_1d.size - self.ndim #degree of freedom
        #initialize fitting params
        #print('-----111-------------',self.ndim)
        #self.get_params_from_model()  #Note this bug, inheritance should be very cautious
        #print('-----222-------------',self.ndim)

    def chi2(self,params,return_mock=False):
        #print('-----',len(params))
        x = self.apply_params_to_model(params)
        try: thislens_mass,thislens_light,thissource = x
        except TypeError: return util.INF_CHI2
        tracer = Tracer_1d(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=self.psf)
        #check posiitons
        if self.positions is not None:
            res = self.check_positions(tracer=tracer)
            if not res: return util.INF_CHI2
        if self.sub_grid is None:
            mock_image_1d = tracer.generate_model_image(self.x_1d,self.y_1d, self.x.shape, self.mask_id)
        else:
            mock_image_1d = tracer.generate_model_image(self.x_1d_sub,self.y_1d_sub, self.xsub.shape, self.mask_id_sub, 
                                                        sub_grid=self.sub_grid, mask_id_binned=self.mask_id)
        if return_mock:
            return np.sum((self.ccd_image_1d-mock_image_1d)**2*self.weight_1d),mock_image_1d
        else:
            return np.sum((self.ccd_image_1d-mock_image_1d)**2*self.weight_1d)