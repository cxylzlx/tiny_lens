class LoadDefalutSetting(object):
    def __init__(self,setting_dir):
        self.setting_dir = setting_dir

    def load_default_setting(self):
        pass

    def load_lens_mass_default_setting(self):
        pass

    def load_lens_light_default_setting(self):
        pass

    def load_source_default_setting(self):
        pass

class Paramter(object):
    """
    parameter object
    :param value: the value of the parameters
    :param low_lim: The hard bound lower limit of parameter's value
    :param up_lim: The hard bound upper limit of parameter's value
    :param prior_type: The prior type of the parameter. we provide three prior types 
                        {0:'uniform ',1:'gaussian',2:'log-uniform'}
    :param prior_0: if prior_type=0 or 2, this is the lower-limit of prior. if prior_type=1. this is 
                    the mean value of the prior
    :param prior_1: if prior_type=0 or 2, this is the upper-limit of prior. if prior_type=1. this is 
                    the standard-deviation value of the prior
    :param fixed: determine whether this parameter is fixed during optimization.
    """
    def __init__(self,value=None, low_lim=None,up_lim=None,prior_type=None,prior_0=None,prior_1=None,fixed=False):
        self.value = value
        self.low_lim = low_lim
        self.up_lim = up_lim
        self.prior_type = prior_type 
        self.prior_0 = prior_0
        self.prior_1 = prior_1
        self.fixed = fixed

    def check_bound(self):
        if (self.value>self.low_lim) and (self.value<self.up_lim):
            return True
        else:
            return False
    
    def __call__(self,value):
        self.value = value

        