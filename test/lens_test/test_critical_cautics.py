import time
import sys; sys.path.append('../../')
from visualizer import visualizer
#from fitter import PymultinestFitRoutine_1d
import pickle
import gzip
from analysis_tool import get_critical_line_and_caustic
from mass import SIELens
#from RayTracing import Tracer

#TODO, why we have a werid feature in the central region(critical line), because singular? 
lens_mass = [SIELens(xc=0.0, yc=0.0, b_sie=1.0, q=0.4, PA=0.0),
            SIELens(xc=0.5, yc=0.5, b_sie=1.0, q=0.8, PA=0.0)
            ]
criticals,caustics = get_critical_line_and_caustic(lens_mass,highresbox=[-3.,3.,-3.,3.],
                                                numres=0.001)

from matplotlib import pyplot as plt 
plt.figure()
for critical in criticals:
    plt.axis('equal')
    plt.plot(critical[:,0],critical[:,1],'k-')
plt.show()

plt.figure()
for caustic in caustics:
    plt.axis('equal')
    plt.plot(caustic[:,0],caustic[:,1],'k-')
plt.show()

#print(criticals)