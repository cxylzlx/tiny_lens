import sys; sys.path.append('../')
from astropy.io import fits
import numpy as np
from parameter import Paramter
from light import Sersic
from mass import SIELens
from matplotlib import pyplot as plt


hw = 100
dpix = 0.05
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
sersic = Sersic(xc=0.1,yc=0.1,I_eff=0.1,r_eff=0.2,n=2.5,q=0.7,PA=110.0)
for i in range(1000):
    image = sersic.get_instensity(x,y)
#--------------------------test sersic

image_norm = fits.open('sersic_image.fits')[0].data
assert(np.all(np.abs(image-image_norm))<1e-8)
#plt.figure()
#
#plt.subplot(131)
#plt.imshow(image,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(132)
#plt.imshow(image_norm,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(133)
#plt.imshow(image-image_norm,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.show()