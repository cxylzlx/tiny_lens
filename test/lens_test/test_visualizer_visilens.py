import time
import sys; sys.path.append('../../')
from visualizer import visualizer
from fitter import PymultinestFitRoutine
import pickle
import gzip

from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util

sie = SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=0.9894401462972869,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]


lens_light = None

source = [Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111,low_lim=-4.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.019934418901511094,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.001,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=0.10569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]

#still use previous fake data
data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=None)

pos =[[-0.17,0.955],[-1.10,-0.515]]
positions = [pos]
fitter = PymultinestFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source_light=source,hw=50,threshhold=1)

thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
print(thislens_light,'----')
from RayTracing import Tracer
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=None)
initial_image = tracer.generate_model_image(fitter.x,fitter.y)

xsrc,ysrc = tracer.ray_shoot(fitter.x,fitter.y)
sgridx, sgridy, _, _ = util.generate_grid(500,0.01)
import numpy as np
I_source = np.zeros(sgridx.shape)
for item in thissource:
    I_source += item.get_instensity(sgridx,sgridy)

if positions is not None:
    for pos_pair in positions:
        posx = np.array([item[0] for item in pos_pair])
        posy = np.array([item[1] for item in pos_pair])
        src_pair = [tracer.ray_shoot(item[0],item[1]) for item in pos_pair]
        srcx = np.array([item[0] for item in src_pair])
        srcy = np.array([item[1] for item in src_pair])

from matplotlib import pyplot as plt 
cbpar = {}
#cbpar['pad'] = 0.02
cbpar['shrink'] = 0.5
default_cmap=plt.get_cmap('jet')
current_cmap=default_cmap
myargs = {'origin':'lower'}
myargs['cmap'] = current_cmap
ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]
myargs['extent'] = ext
plt.figure()
percent = [0,100]
plt.subplot(1,2,1)
vmin = np.percentile(initial_image ,percent[0]) 
vmax = np.percentile(initial_image ,percent[1]) 
plt.imshow(initial_image,vmin=vmin,vmax=vmax,**myargs)
if positions is not None:
    plt.plot(posx,posy,'*',color='k')
cb=plt.colorbar(**cbpar)
cb.ax.minorticks_on()
cb.ax.tick_params(labelsize='small')
plt.title('Data')
plt.xlabel('Arcsec')
plt.ylabel('Arcsec')

plt.subplot(1,2,2)
myargs['extent'] = [sgridx.min(),sgridx.max(),sgridx.min(),sgridx.max()]
#cs = plt.contour(sgridx,sgridy,I_source,colors='k')
#plt.clabel(cs,inline=1, fontsize=10)
vmin = np.percentile(I_source,percent[0]) 
vmax = np.percentile(I_source,percent[1]) 
plt.imshow(I_source,vmin=vmin,vmax=vmax,**myargs)
cb=plt.colorbar(**cbpar)
cb.ax.minorticks_on()
cb.ax.tick_params(labelsize='small')
plt.scatter(xsrc,ysrc,s=0.1,color='k')
if positions is not None:
    plt.plot(srcx,srcy,'*',color='k')
    print('positions in source-plane',srcx,srcy)
plt.title('Chi-square map')
plt.xlabel('Arcsec')
plt.ylabel('Arcsec')
plt.tight_layout()
#plt.savefig('./output.png',dpi=200)
plt.show()