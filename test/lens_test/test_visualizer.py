import time
import sys; sys.path.append('../../')
from visualizer import visualizer
from fitter import PymultinestFitRoutine_1d
import pickle
import gzip

fitter = pickle.load(gzip.open('./pymultinest_fitter.pzip'))

pos =  [[0.77,1.32],[-0.17,-1.10]]  #[[0.75, 1.3],[-0.15, -1.2]]
visualizer(fitter,fitter.best_fit,positions=[pos])

fitter.chi2(fitter.best_fit)
