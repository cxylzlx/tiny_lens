import sys; sys.path.append('../')
from astropy.io import fits
import numpy as np
from parameter import Paramter
from mass import SIELens,ExternalShear
from matplotlib import pyplot as plt


hw = 100
dpix = 0.05
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
shear = ExternalShear(shear=0.1,shearangle=70)
for i in range(100):
    shear.deflect(x,y)
    shear._altered = True
#--------------------------test sersic

image_norm_x = fits.open('shear_deflx.fits')[0].data
image_norm_y = fits.open('shear_defly.fits')[0].data
assert(np.all(np.abs(shear.deflected_x-image_norm_x))<1e-8)
assert(np.all(np.abs(shear.deflected_y-image_norm_y))<1e-8)
#plt.figure()
#
#plt.subplot(131)
#plt.imshow(shear.deflected_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(132)
#plt.imshow(image_norm_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(133)
#plt.imshow(shear.deflected_x-image_norm_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.show()