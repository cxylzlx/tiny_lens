import time
import sys; sys.path.append('../../')
from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from fitter import PymultinestFitRoutine
from matplotlib import pyplot as plt 
from visualizer import visualizer

e1,e2 = util.change_q_theta_to_e1e2(0.9,45)
e1_sh,e2_sh = util.phi_gamma_ellipticity(70,0.1)
lens_mass = [SIELens(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.2,prior_1=0.2,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.2,prior_1=0.2,fixed=False),
    b_sie=Paramter(value=1.2,low_lim=0.0,up_lim=3.0,prior_type=0,prior_0=0.8,prior_1=1.6,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    ),
    ExternalShear(
    e1=Paramter(value=e1_sh,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=e2_sh,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False)
    )]

e1,e2 = util.change_q_theta_to_e1e2(0.8,-45)
lens_light = [Sersic(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=0.4,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.0,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

e1,e2 = util.change_q_theta_to_e1e2(0.7,110)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.2,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=2.5,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]


data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=2)

pos = [[0.77,1.32],[-0.17,-1.10]]   #[[0.75,1.30],[-0.15,-1.20]]
Fitness_pymulti = PymultinestFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source_light=source,
                    hw=50,threshhold=0.5,positions=[pos])
#fitness_value,mock_image = Fitness_pso.chi2(Fitness_pso.params_init,return_mock=True)
fitness_value = Fitness_pymulti.chi2(Fitness_pymulti.params_init,return_mock=False)


#ext = [Fitness_pymulti.x.min(),Fitness_pymulti.x.max(),Fitness_pymulti.x.min(),Fitness_pymulti.x.max()]
#fig = util.matplot_show_fig(Fitness_pymulti.ccd_image,ext=ext)
#posx = [item[0] for item in pos]
#posy = [item[1] for item in pos]
#plt.plot(posx,posy,'*')
#plt.show()


t0 = time.time()
Fitness_pymulti.run()
t1 = time.time()
import pickle
import gzip
pickle.dump(Fitness_pymulti ,gzip.open('pymultinest_fitter_2d_nosub'+'.pzip','wb'))

print('total time elapse:',t1-t0)
print('--------',Fitness_pymulti.best_fit)
visualizer(Fitness_pymulti,Fitness_pymulti.best_fit)



