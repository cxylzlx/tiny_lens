import sys; sys.path.append('../')

import numpy as np
from parameter import Paramter
from light import Sersic
from mass import SIELens
from matplotlib import pyplot as plt


hw = 100
dpix = 0.05
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
sersic = Sersic(xc=0,yc=0,I_eff=1.0,r_eff=0.8,n=0.5,q=0.6,PA=45)
image = sersic.get_instensity(x,y)

plt.figure()
plt.imshow(image,origin='lower',extent=ext,cmap='jet')
plt.colorbar()
plt.show()
plt.close()
#--------------------------test sersic

#--------------------------test sie
sie = SIELens(xc=0.0,yc=0.0,b_sie=1.0,q=0.8,PA=0)
sie.deflect(x,y)
defl_x = sie.deflected_x
defl_y = sie.deflected_y

plt.figure()
plt.subplot(121)
plt.imshow(defl_x,origin='lower',extent=ext,cmap='jet')
plt.colorbar()
plt.subplot(122)
plt.imshow(defl_y,origin='lower',extent=ext,cmap='jet')
plt.colorbar()
plt.show()
plt.close()
#--------------------------test sie