import sys; sys.path.append('../')
from astropy.io import fits
import numpy as np
from parameter import Paramter
from mass import SIELens
from matplotlib import pyplot as plt


hw = 100
dpix = 0.05
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
sie = SIELens(xc=0.0,yc=0.0,b_sie=1.2,q=0.9,PA=45.0)
for i in range(1000):
    sie.deflect(x,y)
    sie._altered = True
#--------------------------test sersic

image_norm_x = fits.open('../data/sie_deflx.fits')[0].data
image_norm_y = fits.open('../data/sie_defly.fits')[0].data
#image_norm_x[100,100] = sie.deflected_x[100,100]
#image_norm_y[100,100] = sie.deflected_y[100,100]
assert(np.all(np.abs(sie.deflected_x-image_norm_x))<1e-8)
assert(np.all(np.abs(sie.deflected_y-image_norm_y))<1e-8)
#plt.figure()
#
#plt.subplot(131)
#plt.imshow(sie.deflected_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(132)
#plt.imshow(image_norm_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.subplot(133)
#plt.imshow(sie.deflected_x-image_norm_x,origin='lower',cmap='jet')
#plt.colorbar()
#
#plt.show()