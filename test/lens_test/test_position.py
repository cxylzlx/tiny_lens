import time
import sys; sys.path.append('../../')
from visualizer import visualizer
from fitter import PymultinestFitRoutine_1d
import pickle
import gzip
from RayTracing import Tracer

fitter = pickle.load(gzip.open('./pymultinest_fitter_2d_nosub.pzip'))
visualizer(fitter,fitter.best_fit)

pos =[[0.75,1.30],[-0.15,-1.20]]
fitter.positions = [pos]

#thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.best_fit)
#tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
print('---',fitter.chi2(fitter.best_fit))

