import pygmo as pg
import time
import sys; sys.path.append('../../')
from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from fitter import GeneticFitRoutine
from RayTracing import Tracer
from matplotlib import pyplot as plt 

lens_mass = [SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=0.9894401462972869,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.8,prior_1=1.6,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    ),
    ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=0.0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False)
    )]

lens_light = None

source = [Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.01993442182283057,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.10569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

#data_dict = util.load_data(image_file='../data/image.fits',weight_file='../data/weight.fits',psf_file='../data/psf.fits',pix_size=0.05)
#print(data_dict)
x,y = util.generate_grid(100,0.05)
from astropy.io import fits
image_norm = fits.getdata('../../data/test_ray_tracing.fits',ext=0,header=False)
psf = fits.getdata('../../data/test_ray_tracing.fits',ext=1,header=False)
#fig = util.plot_image(image_norm,x=x,y=y)
#plt.show(fig)

tracer = Tracer(lens_mass=lens_mass,lens_light=lens_light,source=source,psf=psf)
for i in range(1):
    mock_image = tracer.generate_model_image(x,y)
fig = util.plot_image(mock_image,x=x,y=y)
plt.show(fig)

#fig = util.plot_image(image_norm-mock_image,x=x,y=y)
#plt.show(fig)

#start = time.time()
#print('run genetic-----------------')
#prob_python = pg.problem(lf.Fitness_genetic())
#print(prob_python)
#algo = pg.algorithm(pg.mbh(pg.algorithm(pg.de1220(gen = 500)),stop=3))
#algo.set_verbosity(200)
#pop = pg.population(prob_python, 50)
#pop = algo.evolve(pop)
#end = time.time()
#print('genetic finished')
#print('Total time ellapse is:',end-start)
#print('-------------------------------')
#print('best fit fitnnes: ',pop.champion_f)
#print('best fit parameter value: ',pop.champion_x)       
#print('-------------------------------')