import time
import sys; sys.path.append('../')
from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from modelling_1d import DynestyFitRoutine
from matplotlib import pyplot as plt 


e1,e2 = util.change_q_theta_to_e1e2(0.9,45)
e1_sh,e2_sh = util.phi_gamma_ellipticity(70,0.1)
lens_mass = [SIELens(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.2,prior_1=0.2,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.2,prior_1=0.2,fixed=False),
    b_sie=Paramter(value=1.2,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.8,prior_1=1.6,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    ),
    ExternalShear(
    e1=Paramter(value=e1_sh,low_lim=0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=e2_sh,low_lim=0.0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False)
    )]

e1,e2 = util.change_q_theta_to_e1e2(0.8,-45)
lens_light = [Sersic(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=0.4,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.0,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

e1,e2 = util.change_q_theta_to_e1e2(0.7,110)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.2,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=2.5,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]


data_dict = util.load_data_from_biz(file_name='../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05)

#pos =[[0.75,1.30],[-0.15,-1.20]]
Fitness_dynesty = DynestyFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source=source,positions=None,hw=None)
#fitness_value,mock_image = Fitness_pso.chi2(Fitness_pso.params_init,return_mock=True)
fitness_value = Fitness_dynesty.chi2_1d(Fitness_dynesty.params_init,return_mock=False)


Fitness_dynesty.run()
#print('--------',Fitness_pymulti.best_fit)
util.visualizer(Fitness_dynesty,Fitness_dynesty.best_fit)

