import time
import sys; sys.path.append('../../')
from visualizer import visualizer
from fitter import PymultinestFitRoutine
import pickle
import gzip

from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from RayTracing import Tracer

sie = SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=4.0,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=10.0,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]

lens_light = None

source = [Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111+0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138-0.2,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.019934418901511094,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.001,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=1.30569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]

from matplotlib import pyplot as plt 
import numpy as np

#still use previous fake data
data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=None)
fitter = PymultinestFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source_light=source,hw=250,threshhold=1)
thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
if fitter.sub_grid is None:
    mock_image = tracer.generate_model_image(fitter.x,fitter.y)
else:
    mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
im0 = np.copy(mock_image)


data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=1)
fitter = PymultinestFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source_light=source,hw=250,threshhold=1)
thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
if fitter.sub_grid is None:
    mock_image = tracer.generate_model_image(fitter.x,fitter.y)
else:
    mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
im1 = np.copy(mock_image)


data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=2)
fitter = PymultinestFitRoutine(data_dict=data_dict,lens_mass=lens_mass,lens_light=lens_light,source_light=source,hw=250,threshhold=1)
thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
if fitter.sub_grid is None:
    mock_image = tracer.generate_model_image(fitter.x,fitter.y)
else:
    mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
im2 = np.copy(mock_image)


ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]
plt.figure()
plt.subplot(131)
plt.imshow(im0,origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.subplot(132)
plt.imshow(im1,origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.subplot(133)
plt.imshow(np.abs(im1-im2),origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.show()