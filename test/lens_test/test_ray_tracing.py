import pygmo as pg
import time
import sys; sys.path.append('../../')
from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from fitter import GeneticFitRoutine
from RayTracing import Tracer
from matplotlib import pyplot as plt 

e1,e2 = util.change_q_theta_to_e1e2(0.9,45)
lens_mass = [SIELens(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    b_sie=Paramter(value=1.2,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=3.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    ),
    ExternalShear(
    shear=Paramter(value=0.1,low_lim=0,up_lim=1.0,prior_type=0,prior_0=0.0,prior_1=0.3,fixed=False),
    shearangle=Paramter(value=70.0,low_lim=0.0,up_lim=180.0,prior_type=0,prior_0=0.0,prior_1=180.0,fixed=False)
    )]

lens_light = [Sersic(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.8,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    n=Paramter(value=3.0,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
    e1=Paramter(value=0,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

e1,e2 = util.change_q_theta_to_e1e2(0.7,110)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.2,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    n=Paramter(value=2.5,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

#data_dict = util.load_data(image_file='../data/image.fits',weight_file='../data/weight.fits',psf_file='../data/psf.fits',pix_size=0.05)
#print(data_dict)
x,y = util.generate_grid(100,0.05)
from astropy.io import fits
image_norm = fits.getdata('../../data/test_ray_tracing.fits',ext=0,header=False)
psf = fits.getdata('../../data/test_ray_tracing.fits',ext=1,header=False)
#fig = util.plot_image(image_norm,x=x,y=y)
#plt.show(fig)

tracer = Tracer(lens_mass=lens_mass,lens_light=lens_light,source=source,psf=psf)
for i in range(1):
    mock_image = tracer.generate_model_image(x,y)
fig = util.plot_image(mock_image,x=x,y=y)
plt.show(fig)

#fig = util.plot_image(image_norm-mock_image,x=x,y=y)
#plt.show(fig)

#start = time.time()
#print('run genetic-----------------')
#prob_python = pg.problem(lf.Fitness_genetic())
#print(prob_python)
#algo = pg.algorithm(pg.mbh(pg.algorithm(pg.de1220(gen = 500)),stop=3))
#algo.set_verbosity(200)
#pop = pg.population(prob_python, 50)
#pop = algo.evolve(pop)
#end = time.time()
#print('genetic finished')
#print('Total time ellapse is:',end-start)
#print('-------------------------------')
#print('best fit fitnnes: ',pop.champion_f)
#print('best fit parameter value: ',pop.champion_x)       
#print('-------------------------------')