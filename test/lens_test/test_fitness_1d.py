import pygmo as pg
import time
import sys; sys.path.append('../../')
from mass import SIELens,ExternalShear
from light import Sersic
from parameter import Paramter
import util
from fitter import PSOFitRoutine_1d
from matplotlib import pyplot as plt 
import copy 

e1,e2 = util.change_q_theta_to_e1e2(0.9,45)
lens_mass = [SIELens(
    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    b_sie=Paramter(value=1.2,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=3.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]#,
    #ExternalShear(
    #shear=Paramter(value=0.1,low_lim=0,up_lim=1.0,prior_type=0,prior_0=0.0,prior_1=0.3,fixed=False),
    #shearangle=Paramter(value=70.0,low_lim=0.0,up_lim=180.0,prior_type=0,prior_0=0.0,prior_1=180.0,fixed=False)
    #)]

#lens_light = [Sersic(
#    xc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
#    yc=Paramter(value=0,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
#    I_eff=Paramter(value=0.5,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
#    r_eff=Paramter(value=0.8,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
#    n=Paramter(value=3.0,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
#    e1=Paramter(value=0,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
#    e2=Paramter(value=0,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
#)]

e1,e2 = util.change_q_theta_to_e1e2(0.7,110)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.1,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    r_eff=Paramter(value=0.2,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=3.0,fixed=False),
    n=Paramter(value=2.5,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
)]

data_dict = util.load_data_from_biz(file_name='../../data/SLACSJ000000.00+000000.0_biz.fits',pix_size=0.05,sub_grid=None)
#data_dict = util.load_data(image_file='../data/image.fits',weight_file='../data/weight.fits',psf_file='../data/psf.fits',pix_size=0.05)
fitter = PSOFitRoutine_1d(data_dict=data_dict,lens_mass=lens_mass,source_light=source,hw=None,default_mask_radii=3.0)
params = copy.deepcopy(fitter.params_init)

t0 = time.time()
for i in range(1):
    fitter.fitness(params)
t1 = time.time()
print('total time elapse:',t1-t0)
print('fitness value is',fitter.fitness(params))