import sys; sys.path.append('../../../')
import numpy as np 
import pickle
import gzip
from visibility.visibility_visualizer import visualizer
from matplotlib import pyplot as plt
import sys; sys.path.append('../../../')
from parameter import Paramter
from mass import SIELens,ExternalShear
from light import Sersic
import util
import visibility.visibility_utils as vis_ul
from visibility.fitter_visibility import PymultinestFitRoutine_1d,PymultinestFitRoutine
import time
import copy

setting = vis_ul.input_settings4modelling(
    data_file='../../../data/SPT0346-52_cont.bin',
    hw=100,
    dpix=0.05,
    sub_grid=4,
    modelcal=True)

#print(vis_ul.get_optimium_dpix_and_hw(setting['data']))
dpix,hw = vis_ul.get_optimium_dpix_and_hw(setting['data'])
setting['dpix'] = dpix
setting['hw'] = hw

visi_dataset = copy.deepcopy(setting['data'])
#7 array
u_coord = visi_dataset.u
v_coord = visi_dataset.v 
visi_real = visi_dataset.real
visi_imag = visi_dataset.imag
visi_sigma = visi_dataset.sigma
visi_ant1 = visi_dataset.ant1
visi_ant2 = visi_dataset.ant2
#primary beam FWHM
pbfwhm = visi_dataset.PBfwhm

#show visi-data
plt.figure()
plt.subplot(3,2,1)
plt.scatter(visi_real,visi_imag,s=0.5) 
plt.subplot(3,2,2)
plt.scatter(visi_real,visi_imag,s=0.5,c=visi_sigma,cmap='jet') 
plt.colorbar()
plt.subplot(3,2,3)
plt.plot(np.arange(len(u_coord)),u_coord)
plt.subplot(3,2,4)
plt.plot(np.arange(len(v_coord)),v_coord)
plt.subplot(3,2,5)
plt.scatter(u_coord,v_coord,s=0.2,c=visi_real,cmap='jet')
plt.colorbar()
plt.subplot(3,2,6)
plt.scatter(u_coord,v_coord,s=0.2,c=visi_imag,cmap='jet')
plt.colorbar()
plt.show()

#generate mock par list
#lens par list
num_of_mock = 10
Lm_xc = [0.0]*num_of_mock
Lm_yc = [0.0]*num_of_mock
Lm_b_sie = np.arange(0.9, 0.9+num_of_mock*0.1, 0.05)
Lm_q = np.random.normal(loc=0.8,scale=0.1,size=num_of_mock)
Lm_theta = np.random.uniform(low=0.0,high=180.0,size=num_of_mock)
Lm_shear_amp = np.random.uniform(low=0.0,high=0.3,size=num_of_mock)
Lm_shear_theta = np.random.uniform(low=0.0,high=180.0,size=num_of_mock)
Lm_e1 = []; Lm_e2 = []
Lm_shear_e1=[]; Lm_shear_e2=[]

for i in range(num_of_mock):
    e1,e2 = util.change_q_theta_to_e1e2(Lm_q[i],Lm_theta[i])
    Lm_e1.append(e1);Lm_e2.append(e2)
    e1_sh,e2_sh = util.phi_gamma_ellipticity(Lm_shear_theta[i],Lm_shear_amp[i])
    Lm_shear_e1.append(e1_sh);Lm_shear_e2.append(e2_sh)

#source par list
S_xc = np.random.normal(loc=0.8,scale=0.1,size=num_of_mock)
i=0
sie = SIELens(
    xc=Paramter(value=Lm_xc[i],low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.1,fixed=False),
    yc=Paramter(value=Lm_yc[i],low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.1,fixed=False),
    b_sie=Paramter(value=Lm_b_sie[i],low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=1.0,prior_1=1.5,fixed=False),
    e1=Paramter(value=Lm_e1[i],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=Lm_e2[i],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=Lm_shear_e1[i],low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=Lm_shear_e2[i],low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    #center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]


lens_light = None

e1s,e2s = util.change_q_theta_to_e1e2(0.65,120)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.1,prior_1=0.1,fixed=False),
    yc=Paramter(value=0.3,low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.3,prior_1=0.1,fixed=False),
    I_eff=Paramter(value=0.02,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=0.1,fixed=False),
    r_eff=Paramter(value=0.15,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20,low_lim=0.01,up_lim=10,prior_type=0,prior_0=1.0,prior_1=1.5,fixed=False),
    e1=Paramter(value=e1s,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2s,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]

pos = [[-0.17,0.955],[-1.10,-0.515]]
fitter = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=None)

from RayTracing import Tracer
thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
mock_image = tracer.generate_model_image(fitter.x,fitter.y)
if fitter.sub_grid is None:
    mock_image_highres = tracer.generate_model_image(fitter.x,fitter.y,sub_grid=fitter.sub_grid)
else:
    mock_image_highres = tracer.generate_model_image(fitter.xsub,fitter.ysub,sub_grid=fitter.sub_grid)
immap = np.zeros(fitter.x.shape)
immap[:,:] += mock_image_highres #resize
immap = immap[::-1,:]  #flip the image, to match the inverse transformation of the visibility data

ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]
plt.figure()
plt.imshow(immap,cmap='jet',extent=ext)
plt.colorbar()
plt.show()

immap *= (fitter.x[0,1]-fitter.x[0,0])**2.  #brightness to flux



from visibility.visibility_utils import fft_interpolate,model_cal
mock_visi_data = fft_interpolate(fitter.dset, immap, fitter.x, fitter.y, fitter.ug)
#print(np.median(mock_visi_data.sigma))

sn_level_aim = 4.0
noise_level = 0.5
mock_visi_data.sigma = np.full(fitter.dset.real.shape,noise_level)
origin_sn_level = vis_ul.return_dataset_sn_level(mock_visi_data)
fac = sn_level_aim/origin_sn_level
noise_level /= fac
mock_visi_data.sigma = np.full(fitter.dset.real.shape,noise_level)
print('original SN level',origin_sn_level)
print('rescaled SN level without noise',vis_ul.return_dataset_sn_level(mock_visi_data))
print('rescaled noise level',noise_level)


add_noise = True
if add_noise:
    visi_noise_realization_real = np.random.normal(loc=0.0,scale=noise_level,size=fitter.dset.real.shape)
    visi_noise_realization_imag = np.random.normal(loc=0.0,scale=noise_level,size=fitter.dset.imag.shape)
    mock_visi_data.real += visi_noise_realization_real
    mock_visi_data.imag += visi_noise_realization_imag
    print('rescaled SN level with noise',vis_ul.return_dataset_sn_level(mock_visi_data))
    
show_dirty = False
if show_dirty:
    from visibility.visibility_plot import uvimageslow
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    imsize = 256; pixsize = 0.05 
    data_dirty_image = uvimageslow(mock_visi_data,256, 0.05)

    ext=[-imsize*pixsize/2.,+imsize*pixsize/2.,-imsize*pixsize/2.,+imsize*pixsize/2.]
    f,axarr = plt.subplots(1,1,figsize=(5,5))
    s = ((fitter.dset.sigma**-2.).sum())**-0.5
    im = axarr.imshow(data_dirty_image,interpolation='nearest',extent=ext,cmap='jet')
    divider = make_axes_locatable(axarr)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    f.colorbar(im, cax=cax)  
    plt.show()


vis_ul.write_dset_to_bin_file(mock_visi_data,outfname='./mock_visi_data.bin')
#print('Maximum S/N ratio of obs data:', vis_ul.return_dataset_sn_level(fitter.dset))