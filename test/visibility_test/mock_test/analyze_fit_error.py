from matplotlib import pyplot as plt
from pymultinest.analyse import Analyzer
import json
import numpy as np
#--------------------------------------
import pickle
import gzip
import sys; sys.path.append('../../../')
from visibility.visibility_visualizer import visualizer


output_dir = './output_id141'

dset_name = []
with open('{}/file.txt'.format(output_dir)) as f:
    for line in f:
        dset_name.append(line[0:-2])
#print(dset_name)

abs_path = ["{}/{}/output_multinest/".format(output_dir,item) for item in dset_name]
var_names = json.load(open(abs_path[0]+"params.json"))
ndim = len(var_names)
print('--------',var_names)

fitter_path = ["{}/{}/pymultinest_fitter.pzip".format(output_dir,item) for item in dset_name]

#the parameter that we want to check
params_name_list = [
                'b_sie_lens_mass0', 
                'r_eff_source_light0',
                'xc_lens_mass0',
                'yc_lens_mass0',
                ]

ids_var = [var_names.index(item) for item in params_name_list]
params_init_list = []
median_list = []
sigma3_low_list = []
sigma3_up_list = []


ii=0
for ii in range(len(dset_name)):  
    fitter = pickle.load(gzip.open(fitter_path[ii]))
    params_init = fitter.params_init

    a = Analyzer(ndim, outputfiles_basename=abs_path[ii])
    # get a dictionary containing information
    stats = a.get_stats()
    marg = stats['marginals'] 
    median = [item['median'] for item in marg]
    sigma = [item['sigma'] for item in marg]
    sigma3_low = [item['3sigma'][0] for item in marg]
    sigma3_up = [item['3sigma'][1] for item in marg]

    for jj in range(len(params_name_list)):
        params_init_list.append(params_init[ids_var[jj]])
        median_list.append(median[ids_var[jj]])
        sigma3_low_list.append(sigma3_low[ids_var[jj]])
        sigma3_up_list.append(sigma3_up[ids_var[jj]])
    

#shape: [nsystem, npar]
#print(np.array(median_list).reshape(-1,len(params_name_list)))
median_list = np.array(median_list).reshape(-1,len(params_name_list))
sigma3_low_list = np.array(sigma3_low_list).reshape(-1,len(params_name_list))
sigma3_up_list = np.array(sigma3_up_list).reshape(-1,len(params_name_list))
params_init_list = np.array(params_init_list).reshape(-1,len(params_name_list))


nfig = len(params_name_list)
ncol = 2
nrow = np.ceil(nfig*1.0/ncol).astype('int')
fig, axes = plt.subplots(nrows=nrow,ncols=ncol)
axes_1d = axes.flatten()

#print('hahahahah')
for ind in range(nfig):
    x = np.arange(len(dset_name))+1
    y_init = params_init_list[:,ind]
    y_med = median_list[:,ind]
    y_low = sigma3_low_list[:,ind]
    y_up = sigma3_up_list[:,ind] 
    axes_1d[ind].errorbar(x, y_med-y_init, yerr=[y_med-y_low, y_up-y_med], xerr=None, fmt='o',capthick=2,capsize=5)
    axes_1d[ind].axhline(y=0.0, color='orange', linestyle='-')
    #set plot style
    axes_1d[ind].minorticks_on() #auto minor tick
    axes_1d[ind].tick_params(axis='y', labelcolor='tab:blue')  #set tick style
    axes_1d[ind].set_xticks(x)  #set x mjor tick location
    axes_1d[ind].xaxis.set_tick_params(which='minor', bottom=False)  #close x-minor tick
    axes_1d[ind].set_title('{}'.format(params_name_list[ind]))

    twin_ax =  axes_1d[ind].twinx() 
    twin_ax.plot(x, y_init, 'o', color='tab:red')
    #set plot style
    twin_ax.minorticks_on()
    twin_ax.tick_params(axis='y', labelcolor='tab:red')

plt.show()
