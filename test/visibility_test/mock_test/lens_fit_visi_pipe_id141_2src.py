from matplotlib import pyplot as plt
import sys; sys.path.append('../../../')
from parameter import Paramter
from mass import SIELens,ExternalShear
from light import Sersic
import util

import visibility.visibility_utils as vis_ul
from visibility.fitter_visibility import PymultinestFitRoutine
from visibility.visibility_visualizer import visualizer
import time
import numpy as np 
import pickle
import gzip

fname = open('./mock_data_id141/positions_2src.txt',"r")
name_list = []
position_list = []
for line in fname:
    line = line[0:] #ignore '\n'
    name_tmp,pos_tmp = line.split(';')
    pos_tmp = eval(pos_tmp)
    
    name_list.append(name_tmp)
    position_list.append(pos_tmp)
fname.close()

from astropy.table import Table
t = Table.read('./mock_data_id141/params_2src.csv',format='ascii.csv')

#jj=0
jj = int(sys.argv[1])
print(jj,'------------')
dset_name = name_list[jj]  #-------------
#pos = [[[0.0,0.0],[2.0,2.0]]]
pos = position_list[jj] 
print('position is:------------',pos)
out_dir = './output_id141'  #--------------

setting = vis_ul.input_settings4modelling(
    data_file='./mock_data_id141/{}.bin'.format(dset_name),
    hw=250,
    dpix=0.05,
    sub_grid=None,
    modelcal=True,
    out_dir=out_dir,
    dset_name=dset_name,
    )


dpix,hw = vis_ul.get_optimium_dpix_and_hw(setting['data'])
setting['dpix'] = dpix
setting['hw'] = hw


Lm_xc = t['xc_lens_mass0']
Lm_yc = t['yc_lens_mass0']
Lm_b_sie = t['b_sie_lens_mass0']
Lm_e1 = t['e1_lens_mass0']
Lm_e2 = t['e2_lens_mass0']
Lm_shear_e1 = t['e1_shear_lens_mass']
Lm_shear_e2 = t['e2_shear_lens_mass']

S1_xc = t['xc_source_light0']
S1_yc = t['yc_source_light0']
S1_Ieff = t['I_eff_source_light0']
S1_reff = t['r_eff_source_light0']
S1_n = t['n_source_light0']
S1_e1 = t['e1_source_light0']
S1_e2 = t['e2_source_light0']

S2_xc = t['xc_source_light1']
S2_yc = t['yc_source_light1']
S2_Ieff = t['I_eff_source_light1']
S2_reff = t['r_eff_source_light1']
S2_n = t['n_source_light1']
S2_e1 = t['e1_source_light1']
S2_e2 = t['e2_source_light1']


sie = SIELens(
    xc=Paramter(value=Lm_xc[jj],low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.1,fixed=False),
    yc=Paramter(value=Lm_yc[jj],low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.1,fixed=False),
    b_sie=Paramter(value=Lm_b_sie[jj],low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.5,prior_1=4.0,fixed=False),
    e1=Paramter(value=Lm_e1[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=Lm_e2[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )
shear = ExternalShear(
    e1=Paramter(value=Lm_shear_e1[jj],low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=Lm_shear_e2[jj],low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    #center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]
lens_light = None

source = [
        Sersic(
        xc=Paramter(value=S1_xc[jj],low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
        yc=Paramter(value=S1_yc[jj],low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
        I_eff=Paramter(value=S1_Ieff[jj],low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=0.1,fixed=False),
        r_eff=Paramter(value=S1_reff[jj],low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=0.5,fixed=False),
        n=Paramter(value=S1_n[jj],low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
        e1=Paramter(value=S1_e1[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
        e2=Paramter(value=S1_e2[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
        ),
        Sersic(
                xc=Paramter(value=S2_xc[jj],low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
                yc=Paramter(value=S2_yc[jj],low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
                I_eff=Paramter(value=S2_Ieff[jj],low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=0.1,fixed=False),
                r_eff=Paramter(value=S2_reff[jj],low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=0.5,fixed=False),
                n=Paramter(value=S2_n[jj],low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=8.0,fixed=False),
                e1=Paramter(value=S2_e1[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
                e2=Paramter(value=S2_e2[jj],low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
        )
    ]


Fitness_pymulti = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=pos,threshhold=0.3)
#fitness_value,mock_image = Fitness_pso.chi2(Fitness_pso.params_init,return_mock=True)
fitness_value = Fitness_pymulti.chi2(Fitness_pymulti.params_init,return_mock=False)
print('initial fitness value is',fitness_value)

#visualizer(Fitness_pymulti ,Fitness_pymulti.params_init,positions=Fitness_pymulti.positions,limits=[-4,4,-4,4],mapcontours=np.array([-25,-5,5,25,45,65,85,105,125]))

t0 = time.time()
Fitness_pymulti.run()
t1 = time.time()
print('total time elapse:',t1-t0)
print('--------',Fitness_pymulti.best_fit)
visualizer(Fitness_pymulti ,Fitness_pymulti.best_fit,positions=pos,outfile='{}/{}'.format(out_dir,dset_name),
            limits=[-4,4,-4,4],mapcontours=np.array([-25,-5,5,25,45,65,85,105,125]))
pickle.dump(Fitness_pymulti ,gzip.open('{}/{}/pymultinest_fitter.pzip'.format(out_dir,dset_name),'wb'))
