from matplotlib import pyplot as plt
import sys; sys.path.append('../../../')
from parameter import Paramter
from mass import SIELens,ExternalShear
from light import Sersic
import util

import visibility.visibility_utils as vis_ul
from visibility.fitter_visibility import PymultinestFitRoutine_1d,PymultinestFitRoutine
from visibility.visibility_visualizer import visualizer
import time
import numpy as np 
import pickle
import gzip

setting = vis_ul.input_settings4modelling(
    data_file='./mock_visi_data.bin',
    hw=250,
    dpix=0.05,
    sub_grid=None,
    modelcal=True)

e1,e2 = util.change_q_theta_to_e1e2(0.8,45)
e1_sh,e2_sh = util.phi_gamma_ellipticity(70,0.1)
sie = SIELens(
    xc=Paramter(value=0.0,low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.3,fixed=False),
    yc=Paramter(value=0.0,low_lim=-5.0,up_lim=5.0,prior_type=1,prior_0=0.0,prior_1=0.3,fixed=False),
    b_sie=Paramter(value=1.2,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=2.0,fixed=False),
    e1=Paramter(value=e1,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=e1_sh,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=e2_sh,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    #center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]


lens_light = None

e1s,e2s = util.change_q_theta_to_e1e2(0.65,120)
source = [Sersic(
    xc=Paramter(value=0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-2.0,prior_1=2.0,fixed=False),
    yc=Paramter(value=0.3,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-2.0,prior_1=2.0,fixed=False),
    I_eff=Paramter(value=0.02,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.0,prior_1=0.1,fixed=False),
    r_eff=Paramter(value=0.15,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=2.0,fixed=False),
    e1=Paramter(value=e1s,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=e2s,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]

pos = [  [[0.7099, 1.5415],[0.2178, -0.8993]]  ]
Fitness_pymulti = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=pos,threshhold=0.5)
#fitness_value,mock_image = Fitness_pso.chi2(Fitness_pso.params_init,return_mock=True)
fitness_value = Fitness_pymulti.chi2(Fitness_pymulti.params_init,return_mock=False)
print('initial fitness value is',fitness_value)

#visualizer(Fitness_pymulti ,Fitness_pymulti.params_init,positions=Fitness_pymulti.positions,limits=[-4,4,-4,4],mapcontours=np.array([-25,-5,5,25,45,65,85,105,125]))

t0 = time.time()
Fitness_pymulti.run()
t1 = time.time()
print('total time elapse:',t1-t0)
print('--------',Fitness_pymulti.best_fit)
visualizer(Fitness_pymulti ,Fitness_pymulti.best_fit,positions=pos,
            limits=[-4,4,-4,4],mapcontours=np.array([-25,-5,5,25,45,65,85,105,125]))
pickle.dump(Fitness_pymulti ,gzip.open('pymultinest_fitter'+'.pzip','wb'))
