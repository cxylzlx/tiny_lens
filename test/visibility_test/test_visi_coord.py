#from matplotlib import pyplot as plt
import sys; sys.path.append('../../')
import visibility.visibility_utils as vis_ul
from visibility.visibility_utils import GenerateVisilensGrid,generate_uv_grid_from_image

setting = vis_ul.input_settings4modelling(
    data_file='../../data/SPT0346-52_cont.bin',
    xmax=12.5,
    highresbox=[-2.5, +1.5, -2, +2],
    fieldres=0.05,
    emitres=0.025,
    modelcal=True)
xmapfield,ymapfield,xmapemission,ymapemission,indices = GenerateVisilensGrid(data=setting['data'],xmax=setting['xmax'],
                    emissionbox=setting['highresbox'],fieldres=setting['fieldres'],
                    emitres=setting['emitres'])
print(xmapfield)
print(xmapfield.shape)
print(xmapemission)
print(xmapemission.shape)
