import sys; sys.path.append('../../')
from astropy.io import fits
import numpy as np
from parameter import Paramter
from mass import SIELens,ExternalShear
from matplotlib import pyplot as plt


hw = 100
dpix = 0.025
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
sie = SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=0.9894401462972869,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

sie.deflect(x,y)
sie._altered = True
#--------------------------test sersic

np.save('/home/cao/Desktop/tmp/tiny_lens/sie_map_x.npy',sie.deflected_x)
np.save('/home/cao/Desktop/tmp/tiny_lens/sie_map_y.npy',sie.deflected_y)


shear = ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=0.0,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    center=(sie.xc.value,sie.yc.value)
    )

shear.deflect(x,y)
np.save('/home/cao/Desktop/tmp/tiny_lens/shear_map_x.npy',shear.deflected_x)
np.save('/home/cao/Desktop/tmp/tiny_lens/shear_map_y.npy',shear.deflected_y)
