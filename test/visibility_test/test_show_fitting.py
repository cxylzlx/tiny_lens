import sys; sys.path.append('../../')
import numpy as np 
import pickle
import gzip
from visibility.visibility_visualizer import visualizer

fitter = pickle.load(gzip.open('./pso_fitter.pzip'))
visualizer(fitter ,fitter.best_fit,imsize=300,pixsize=0.07,
    limits=[-4,4,-4,4],mapcontours=np.array([-25,-5,5,25,45,65,85,105,125]))