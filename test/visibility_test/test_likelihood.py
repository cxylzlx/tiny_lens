from matplotlib import pyplot as plt
import sys; sys.path.append('../../')
from parameter import Paramter
from mass import SIELens,ExternalShear
from light import Sersic
import util

import visibility.visibility_utils as vis_ul
from visibility.fitter_visibility import PymultinestFitRoutine_1d,PymultinestFitRoutine
import time
import numpy as np 

#import pickle
#import gzip
#input_value = pickle.load(gzip.open('../../data/chains_SPT0346-52_modelcont.pzip'))


setting = vis_ul.input_settings4modelling(
    data_file='../../data/SPT0346-52_cont.bin',
    hw=250,
    dpix=0.05,
    sub_grid=None, #25,
    modelcal=True)

sie = SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=0.9894401462972869,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]


#hw = 100
#dpix = 0.025
#coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
#x,y = np.meshgrid(coord,coord)
#ext = [coord.min(),coord.max(),coord.min(),coord.max()]
#shear.deflect(x,y)
#np.save('/home/cao/Desktop/tmp/tiny_lens/shear_map_x.npy',shear.deflected_x)
#np.save('/home/cao/Desktop/tmp/tiny_lens/shear_map_y.npy',shear.deflected_y)


lens_light = None

source = [Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.019934418901511094,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.001,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=0.10569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]


fitter = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=None)


t0 = time.time()
for i in range(1):
    fitness_value = fitter.chi2(fitter.params_init,return_mock=False)
t1 = time.time()
print('total time elapse:',t1-t0)
print('fitness value is',fitness_value[0])