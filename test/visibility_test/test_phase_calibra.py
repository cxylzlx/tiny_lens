from matplotlib import pyplot as plt
import sys; sys.path.append('../../')
from visibility.visibility_plot import uvimageslow
import visibility.visibility_data as vl 
import time
import numpy as np 

data = vl.read_visdata('../../data/SPT0346-52_cont.bin')