from matplotlib import pyplot as plt
import sys; sys.path.append('../../')
from parameter import Paramter
from mass import SIELens,ExternalShear
from light import Sersic
import util

import visibility.visibility_utils as vis_ul
from visibility.fitter_visibility import PymultinestFitRoutine
import time
import numpy as np 
from RayTracing import Tracer

sie = SIELens(
    xc=Paramter(value=-0.5752766788826056,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    yc=Paramter(value=-0.3621625504040734,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-1.0,prior_1=1.0,fixed=False),
    b_sie=Paramter(value=4.0,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.1,prior_1=10.0,fixed=False),
    e1=Paramter(value=-0.30313906379374145,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.22119082964924078,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

shear = ExternalShear(
    e1=Paramter(value=-0.02984370769248877,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    e2=Paramter(value=-0.1037168695280863,low_lim=-0.5,up_lim=0.5,prior_type=0,prior_0=-0.3,prior_1=0.3,fixed=False),
    center=(sie.xc.value,sie.yc.value)
    )
lens_mass = [sie,shear]

lens_light = None

source = [Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111+0.1,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138-0.2,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.019934418901511094,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.001,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=1.30569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )]



setting = vis_ul.input_settings4modelling(
    data_file='../../data/SPT0346-52_cont.bin',
    hw=150,
    dpix=0.05,
    sub_grid=None, #25,
    modelcal=True)

fitter = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=None)

fitness_value = fitter.chi2(fitter.params_init,return_mock=False)
print('fitness value is',fitness_value[0])


thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
if fitter.sub_grid is None:
    mock_image = tracer.generate_model_image(fitter.x,fitter.y)
else:
    mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
im0 = np.copy(mock_image)

#-----------------------------------------------------------------------------------------------------

setting = vis_ul.input_settings4modelling(
    data_file='../../data/SPT0346-52_cont.bin',
    hw=150,
    dpix=0.05,
    sub_grid=1, #25,
    modelcal=True)

fitter = PymultinestFitRoutine(data_dict=setting,lens_mass=lens_mass,lens_light=None,source_light=source,
                    positions=None)

fitness_value = fitter.chi2(fitter.params_init,return_mock=False)
print('fitness value is',fitness_value[0])


thislens_mass,thislens_light,thissource = fitter.apply_params_to_model(fitter.params_init)
tracer = Tracer(lens_mass=thislens_mass,lens_light=thislens_light,source=thissource,psf=fitter.psf)
if fitter.sub_grid is None:
    mock_image = tracer.generate_model_image(fitter.x,fitter.y)
else:
    mock_image = tracer.generate_model_image(fitter.xsub,fitter.ysub, fitter.sub_grid)
    #mock_image = tracer.generate_model_image(fitter.x,fitter.y, fitter.sub_grid) #compare to this one
im1 = np.copy(mock_image)

print(fitter.x.shape,'------',fitter.xsub.shape)
#print(fitter.x[0:10,0])
#print(fitter.xsub[0:10,0])

ext = [fitter.x.min(),fitter.x.max(),fitter.y.min(),fitter.y.max()]

plt.figure()
plt.subplot(131)
plt.imshow(im0,origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.subplot(132)
plt.imshow(im1,origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.subplot(133)
plt.imshow(np.abs(im1-im0),origin='lower',cmap='jet',extent=ext)
plt.colorbar()
plt.show()


dist = np.sqrt((fitter.x-fitter.xsub)**2+(fitter.y-fitter.ysub)**2)
print('maxmium distance is',dist.max())

#plt.figure()
#plt.hist(dist,bins='auto')
##plt.scatter(fitter.x,fitter.y,s=0.1,color='red')
##plt.scatter(fitter.xsub,fitter.ysub,s=0.1,color='blue')
#plt.show()