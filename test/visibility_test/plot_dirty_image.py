from matplotlib import pyplot as plt
import sys; sys.path.append('../../')
from visibility.visibility_plot import uvimageslow
import visibility.visibility_data as vl 
import time
import numpy as np 

data = vl.read_visdata('../../data/SPT0346-52_cont.bin')

t0 = time.time()
imdata = uvimageslow(data,300, 0.07)
t1 = time.time()
print('total time elapse:',t1-t0)
#imdata = np.flip(imdata,(0,1))

kargs = {'interpolation':'nearest','cmap':'jet'} #,'origin':'lower'
fig,ax = plt.subplots()
ax.imshow(imdata,**kargs)
plt.show()
