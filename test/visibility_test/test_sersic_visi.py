import sys; sys.path.append('../../')
from astropy.io import fits
import numpy as np
from parameter import Paramter
from light import Sersic
from mass import SIELens
from matplotlib import pyplot as plt


hw = 100
dpix = 0.025
coord = np.arange(-hw*dpix,(hw+1)*dpix,dpix)
x,y = np.meshgrid(coord,coord)
ext = [coord.min(),coord.max(),coord.min(),coord.max()]

#--------------------------test sersic
sersic  = Sersic(
    xc=Paramter(value=-0.5752766788826056+0.21578111,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    yc=Paramter(value=-0.3621625504040734+0.28592138,low_lim=-5.0,up_lim=5.0,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    I_eff=Paramter(value=0.019934418901511094,low_lim=0.0,up_lim=10.0,prior_type=0,prior_0=0.001,prior_1=1.0,fixed=False),
    r_eff=Paramter(value=0.10569824,low_lim=0.0,up_lim=5.0,prior_type=0,prior_0=0.01,prior_1=1.0,fixed=False),
    n=Paramter(value=1.20120329,low_lim=0.01,up_lim=10,prior_type=0,prior_0=0.3,prior_1=4.0,fixed=False),
    e1=Paramter(value=-0.03713219,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False),
    e2=Paramter(value=0.12273177,low_lim=-1,up_lim=1,prior_type=0,prior_0=-0.5,prior_1=0.5,fixed=False)
    )

for i in range(1):
    image = sersic.get_instensity(x,y)
#--------------------------test sersic


np.save('/home/cao/Desktop/tmp/tiny_lens/source_tiny.npy',image)



