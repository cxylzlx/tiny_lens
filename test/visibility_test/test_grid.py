import sys; sys.path.append('../../')
import visibility.visibility_utils as vis_ul

data_dict = vis_ul.input_settings4modelling(
    data_file='../../data/SPT0346-52_cont.bin',
    hw=3,
    dpix=0.05,
    sub_grid=1,
    modelcal=True)

dset = data_dict['data']
hw = data_dict['hw']
dpix = data_dict['dpix'] 
modelcal = data_dict['modelcal']
sub_grid = data_dict['sub_grid']

x,y,xsub,ysub =  vis_ul.GenerateVisilensGrid(
    data=dset,
    hw=hw,
    dpix=dpix,
    sub_grid=sub_grid
)

import numpy as np
arcsec2rad = np.pi/(180.*3600.)
rad2arcsec =3600.*180./np.pi
print(x+0.05)
print(xsub+0.05)
#print(x.shape)
#print(xsub.shape)